<?php

namespace AppBundle\Form;



use AppBundle\Entity\AnneeRepository;
use AppBundle\Entity\ClasseRepository;
use AppBundle\Entity\FiliereRepository;
use AppBundle\Entity\MatiereRepository;
use AppBundle\Entity\NiveauRepository;
use AppBundle\Entity\SemestreRepository;
use AppBundle\Entity\UserRepository;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ActualiteForm extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('classe', EntityType::class, array(
                'class' => 'AppBundle:Classe',
                'query_builder' => function (ClasseRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->join('u.annee','a')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.nom', 'ASC');
                },
                'choice_label' => function ($classe) {
                    return $classe->getAnnee()->getNom(). '   ||     ' . $classe->getNom();
                },
                'required' =>true,
            ))
            ->add('user', EntityType::class, array(
                'class' => 'AppBundle:User',
                'query_builder' => function (UserRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.nom', 'ASC');
                },
                'choice_label' => 'nom',
                'required' =>true,
            ))
            ->add('titre')
            ->add('text')
            ->add('date', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ))
            ->add('type', ChoiceType::class, array(
                'choices'  => array(
                    'Classe' => 'Classe',
                    'Etudiant' => 'Etudiant',
                    'Enseignant' => 'Enseignant',
                ),
            ))

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Actualite'
        ));
    }

    public function getName()
    {
        return 'summernote_1';
    }

}
