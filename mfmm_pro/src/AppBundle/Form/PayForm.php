<?php

namespace AppBundle\Form;



use AppBundle\Entity\AnneeRepository;
use AppBundle\Entity\ClasseRepository;
use AppBundle\Entity\DevoirRepository;
use AppBundle\Entity\FiliereRepository;
use AppBundle\Entity\MatiereRepository;
use AppBundle\Entity\NiveauRepository;
use AppBundle\Entity\SemestreRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PayForm extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           ->add('prix')
            ->add('date', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Paiement'
        ));
    }

    public function getName()
    {
        return 'paiement_form';
    }

}
