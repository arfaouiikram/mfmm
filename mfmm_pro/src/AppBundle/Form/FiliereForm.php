<?php

namespace AppBundle\Form;



use AppBundle\Entity\AnneeRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FiliereForm extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('nomA')
            ->add('nomE')
            ->add('code')
           /* ->add('annee', EntityType::class, array(
                'class' => 'AppBundle:Annee',
                'query_builder' => function (AnneeRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.nom', 'ASC');
                },
                'choice_label' => 'nom',))*/
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Filiere'
        ));
    }

    public function getName()
    {
        return 'filiere_form';
    }

}
