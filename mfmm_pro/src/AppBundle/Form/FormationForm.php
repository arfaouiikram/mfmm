<?php

namespace AppBundle\Form;



use AppBundle\Entity\FiliereRepository;
use AppBundle\Entity\SalleRepository;
use AppBundle\Entity\SousFiliereRepository;
use AppBundle\Entity\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormationForm extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('dateD')
            ->add('dateF')
            ->add('dateD', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ))
            ->add('dateF', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ))
            ->add('sfiliere', EntityType::class, array(
                'class' => 'AppBundle:SousFiliere',
                'query_builder' => function (SousFiliereRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.nom', 'ASC');
                },
                'choice_label' => 'nom',))
            ->add('Formateur', EntityType::class, array(
                'class' => 'AppBundle:User',
                'query_builder' => function (UserRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->andWhere('u.roles LIKE :role')
                        ->orderBy('u.nom', 'ASC')
                        ->setParameter('role','%"ROLE_ENSEIGNANT"%');
                },
                'choice_label' => 'nom',
                'multiple'=>true))
            ->add('Stagiaire', EntityType::class, array(
                'class' => 'AppBundle:User',
                'query_builder' => function (UserRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->andWhere('u.roles LIKE :role')
                        ->orderBy('u.nom', 'ASC')
                        ->setParameter('role','%"ROLE_ETUDIANT"%');
                },
                'choice_label' => 'nom',
                'multiple'=>true))
           /* ->add('salle', EntityType::class, array(
                'class' => 'AppBundle:Salle',
                'query_builder' => function (SalleRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.nom', 'ASC');
                },
                'choice_label' => 'nom',))*/
            ->add('lieu')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Formation'
        ));
    }

    public function getName()
    {
        return 'formation_form';
    }

}
