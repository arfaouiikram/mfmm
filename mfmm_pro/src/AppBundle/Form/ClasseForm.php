<?php

namespace AppBundle\Form;



use AppBundle\Entity\AnneeRepository;
use AppBundle\Entity\FiliereRepository;
use AppBundle\Entity\NiveauRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ClasseForm extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('annee', EntityType::class, array(
                'class' => 'AppBundle:Annee',
                'query_builder' => function (AnneeRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.nom', 'ASC');
                },
                'choice_label' => 'nom',))
            ->add('niveau', EntityType::class, array(
                'class' => 'AppBundle:Niveau',
                'query_builder' => function (NiveauRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.nom', 'ASC');
                },
                'choice_label' => 'nom',))
            ->add('filiere', EntityType::class, array(
                'class' => 'AppBundle:Filiere',
                'query_builder' => function (FiliereRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.nom', 'ASC');
                },
                'choice_label' => 'nom',))
            ->add('prix')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Classe'
        ));
    }

    public function getName()
    {
        return 'classe_form';
    }

}
