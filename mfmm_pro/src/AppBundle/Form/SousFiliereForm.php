<?php

namespace AppBundle\Form;



use AppBundle\Entity\FiliereRepository;
use AppBundle\Entity\SousFiliereRepository;
use AppBundle\Entity\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SousFiliereForm extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('nomA')
            ->add('nomE')
            ->add('dateD')
            ->add('dateF')
            ->add('nbJour')
            ->add('code')
            /*->add('dateD', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ))
            ->add('dateF', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ))*/
            ->add('code')
            ->add('SousFilereFiliere', EntityType::class, array(
                'class' => 'AppBundle:Filiere',
                'multiple'=>'true',
                'query_builder' => function (FiliereRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.nom', 'ASC');
                },
                'choice_label' => 'nom',))

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\SousFiliere'
        ));
    }

    public function getName()
    {
        return 'sous_filiere_form';
    }

}
