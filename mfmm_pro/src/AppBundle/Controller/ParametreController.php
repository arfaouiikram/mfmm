<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Annee;
use AppBundle\Entity\Classe;
use AppBundle\Entity\Devoir;
use AppBundle\Entity\Filiere;
use AppBundle\Entity\Formation;
use AppBundle\Entity\Groupe;
use AppBundle\Entity\Matiere;
use AppBundle\Entity\Niveau;
use AppBundle\Entity\Salle;
use AppBundle\Entity\Seance;
use AppBundle\Entity\Semestre;
use AppBundle\Entity\SousFiliere;
use AppBundle\Form\AnneeForm;
use AppBundle\Form\ClasseForm;
use AppBundle\Form\DevoirForm;
use AppBundle\Form\FiliereForm;
use AppBundle\Form\FormationForm;
use AppBundle\Form\GroupeForm;
use AppBundle\Form\MatiereForm;
use AppBundle\Form\NiveauForm;
use AppBundle\Form\SalleForm;
use AppBundle\Form\SeanceForm;
use AppBundle\Form\SemestreForm;
use AppBundle\Form\SousFiliereForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ParametreController extends Controller
{

    public function listNiveauxAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $niveaux = $em->getRepository('AppBundle:Niveau')->findBy(array('isDeleted'=>1));
        return $this->render("@AppBundle/Resources/views/niveau/list.html.twig",array('pagination' => $niveaux));
    }

    public function ajoutNiveauAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $niveau=new Niveau();
        $form = $this->createForm(NiveauForm::class, $niveau);
        $form->handleRequest($request);
        if($request->getMethod()=="POST"){
            $niveau=$form->getData();
            $niveau->setIsDeleted(1);
            $em->persist($niveau);
            $em->flush();
            return $this->redirectToRoute("mfmm_crm_list_niveaux");
        }
        return $this->render("@AppBundle/Resources/views/niveau/ajout.html.twig",array('form' => $form->createView()));
    }

    public function modifierNiveauAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $niveau = $em->getRepository('AppBundle:Niveau')->find($id);
        $form = $this->createForm(NiveauForm::class, $niveau);
        $form->handleRequest($request);
        if($request->getMethod()=="POST"){
            $niveau=$form->getData();
            $em->persist($niveau);
            $em->flush();
            return $this->redirectToRoute("mfmm_crm_list_niveaux");
        }
        return $this->render("@AppBundle/Resources/views/niveau/ajout.html.twig",array('form' => $form->createView()));
    }

    public function supprimerNiveauAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $niveau = $em->getRepository('AppBundle:Niveau')->find($id);
        $niveau->setIsDeleted(0);
        $em->persist($niveau);
        $em->flush();
        return $this->redirectToRoute("mfmm_crm_list_niveaux");

    }

    public function listSalleAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $salle = $em->getRepository('AppBundle:Salle')->findBy(array('isDeleted'=>1));
        return $this->render("@AppBundle/Resources/views/salle/list.html.twig",array('pagination' => $salle));
    }

    public function ajoutSalleAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $salle=new Salle();
        $form = $this->createForm(SalleForm::class, $salle);
        $form->handleRequest($request);
        if($request->getMethod()=="POST"){
            $salle=$form->getData();
            $salle->setIsDeleted(1);
            $em->persist($salle);
            $em->flush();
            return $this->redirectToRoute("mfmm_crm_list_salle");
        }
        return $this->render("@AppBundle/Resources/views/salle/ajout.html.twig",array('form' => $form->createView()));
    }

    public function modifierSalleAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $salle = $em->getRepository('AppBundle:Salle')->find($id);
        $form = $this->createForm(SalleForm::class, $salle);
        $form->handleRequest($request);
        if($request->getMethod()=="POST"){
            $salle=$form->getData();
            $em->persist($salle);
            $em->flush();
            return $this->redirectToRoute("mfmm_crm_list_salle");
        }
        return $this->render("@AppBundle/Resources/views/salle/ajout.html.twig",array('form' => $form->createView()));
    }

    public function supprimerSalleAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $salle = $em->getRepository('AppBundle:Salle')->find($id);
        $salle->setIsDeleted(0);
        $em->persist($salle);
        $em->flush();
        return $this->redirectToRoute("mfmm_crm_list_salle");

    }



    public function listAnneeAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $annee = $em->getRepository('AppBundle:Annee')->findBy(array('isDeleted'=>1));
        return $this->render("@AppBundle/Resources/views/annee/list.html.twig",array('pagination' => $annee));
    }

    public function ajoutAnneeAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $annee=new Annee();
        $form = $this->createForm(AnneeForm::class, $annee);
        $form->handleRequest($request);
        if($request->getMethod()=="POST"){
            $annee=$form->getData();
            $annee->setNom($annee->__toString());
            $annee->setIsDeleted(1);
            $em->persist($annee);
            $em->flush();
            return $this->redirectToRoute("mfmm_crm_list_annees");
        }
        return $this->render("@AppBundle/Resources/views/annee/ajout.html.twig",array('form' => $form->createView()));
    }

    public function modifierAnneeAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $annee = $em->getRepository('AppBundle:Annee')->find($id);
        $form = $this->createForm(AnneeForm::class, $annee);
        $form->handleRequest($request);
        if($request->getMethod()=="POST"){
            $annee=$form->getData();
            $annee->setNom($annee->__toString());
            $em->persist($annee);
            $em->flush();
            return $this->redirectToRoute("mfmm_crm_list_annees");
        }
        return $this->render("@AppBundle/Resources/views/annee/ajout.html.twig",array('form' => $form->createView()));
    }

    public function supprimerAnneeAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $annee = $em->getRepository('AppBundle:Annee')->find($id);
        $annee->setIsDeleted(0);
        $em->persist($annee);
        $em->flush();
        return $this->redirectToRoute("mfmm_crm_list_annees");

    }

    public function listSeanceAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $seance = $em->getRepository('AppBundle:Seance')->findBy(array('isDeleted'=>1));
        return $this->render("@AppBundle/Resources/views/seance/list.html.twig",array('pagination' => $seance));
    }

    public function ajoutSeanceAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $seance=new Seance();
        $form = $this->createForm(SeanceForm::class, $seance);
        $form->handleRequest($request);
        if($request->getMethod()=="POST"){
            $seance=$form->getData();
            $seance->setNom($seance->__toString());
            $seance->setIsDeleted(1);
            $em->persist($seance);
            $em->flush();
            return $this->redirectToRoute("mfmm_crm_list_seances");
        }
        return $this->render("@AppBundle/Resources/views/seance/ajout.html.twig",array('form' => $form->createView()));
    }

    public function modifierSeanceAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $seance = $em->getRepository('AppBundle:Seance')->find($id);
        $form = $this->createForm(SeanceForm::class, $seance);
        $form->handleRequest($request);
        if($request->getMethod()=="POST"){
            $seance=$form->getData();
            $seance->setNom($seance->__toString());
            $em->persist($seance);
            $em->flush();
            return $this->redirectToRoute("mfmm_crm_list_seances");
        }
        return $this->render("@AppBundle/Resources/views/seance/ajout.html.twig",array('form' => $form->createView()));
    }

    public function supprimerSeanceAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $seance = $em->getRepository('AppBundle:Seance')->find($id);
        $seance->setIsDeleted(0);
        $em->persist($seance);
        $em->flush();
        return $this->redirectToRoute("mfmm_crm_list_seances");

    }

    public function listSemestreAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $semstres = $em->getRepository('AppBundle:Semestre')->findBy(array('isDeleted'=>1));
        return $this->render("@AppBundle/Resources/views/semestre/list.html.twig",array('pagination' => $semstres));
    }

    public function ajoutSemestreAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $semestre=new Semestre();
        $form = $this->createForm(SemestreForm::class, $semestre);
        $form->handleRequest($request);
        if($request->getMethod()=="POST"){
            $semestre=$form->getData();
            $semestre->setIsDeleted(1);
            $em->persist($semestre);
            $em->flush();
            return $this->redirectToRoute("mfmm_crm_list_semestres");
        }
        return $this->render("@AppBundle/Resources/views/semestre/ajout.html.twig",array('form' => $form->createView()));
    }

    public function modifierSemestreAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $semestre = $em->getRepository('AppBundle:Semestre')->find($id);
        $form = $this->createForm(SemestreForm::class, $semestre);
        $form->handleRequest($request);
        if($request->getMethod()=="POST"){
            $semestre=$form->getData();
            $em->persist($semestre);
            $em->flush();
            return $this->redirectToRoute("mfmm_crm_list_semestres");
        }
        return $this->render("@AppBundle/Resources/views/semestre/ajout.html.twig",array('form' => $form->createView()));
    }

    public function supprimerSemestreAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $semestre = $em->getRepository('AppBundle:Semestre')->find($id);
        $semestre->setIsDeleted(0);
        $em->persist($semestre);
        $em->flush();
        return $this->redirectToRoute("mfmm_crm_list_semestres");

    }

    public function listGroupeAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $groupe = $em->getRepository('AppBundle:Groupe')->findBy(array('isDeleted'=>1));
        return $this->render("@AppBundle/Resources/views/groupe/list.html.twig",array('pagination' => $groupe));
    }

    public function ajoutGroupeAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $groupe=new Groupe();
        $form = $this->createForm(GroupeForm::class, $groupe);
        $form->handleRequest($request);
        if($request->getMethod()=="POST"){
            $groupe=$form->getData();
            $groupe->setIsDeleted(1);
            $em->persist($groupe);
            $em->flush();
            return $this->redirectToRoute("mfmm_crm_list_groupes");
        }
        return $this->render("@AppBundle/Resources/views/groupe/ajout.html.twig",array('form' => $form->createView()));
    }

    public function modifierGroupeAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $groupe = $em->getRepository('AppBundle:Groupe')->find($id);
        $form = $this->createForm(GroupeForm::class, $groupe);
        $form->handleRequest($request);
        if($request->getMethod()=="POST"){
            $groupe=$form->getData();
            $em->persist($groupe);
            $em->flush();
            return $this->redirectToRoute("mfmm_crm_list_groupes");
        }
        return $this->render("@AppBundle/Resources/views/groupe/ajout.html.twig",array('form' => $form->createView()));
    }

    public function supprimerGroupeAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $groupe = $em->getRepository('AppBundle:Groupe')->find($id);
        $groupe->setIsDeleted(0);
        $em->persist($groupe);
        $em->flush();
        return $this->redirectToRoute("mfmm_crm_list_groupes");

    }

    public function listFiliereAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $filiere = $em->getRepository('AppBundle:Filiere')->findBy(array('isDeleted'=>1));
        return $this->render("@AppBundle/Resources/views/filiere/list.html.twig",array('pagination' => $filiere));
    }
    public function listSousFiliereAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $sousfiliere = $em->getRepository('AppBundle:SousFiliere')->findBy(array('isDeleted'=>1));
        return $this->render("@AppBundle/Resources/views/sousfiliere/list.html.twig",array('pagination' => $sousfiliere));
    }

    public function listFormationAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $formations = $em->getRepository('AppBundle:Formation')->findBy(array('isDeleted'=>1));
        return $this->render("@AppBundle/Resources/views/formation/list.html.twig",array('pagination' => $formations));
    }

    public function listFormationIdAction(Request $request ,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $sousFiliere = $em->getRepository('AppBundle:SousFiliere')->find($id);
        $formations = $em->getRepository('AppBundle:Formation')->findBy(array('isDeleted'=>1,'sfiliere'=>$sousFiliere));
        return $this->render("@AppBundle/Resources/views/formation/list.html.twig",array('pagination' => $formations));
    }

    public function stagiairesAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $formations = $em->getRepository('AppBundle:Formation')->find($id);
        $theme=$formations->getSfiliere();
        $sousFiliere = $em->getRepository('AppBundle:SousFiliere')->find($formations->getSfiliere()->getId());
        return $this->render("@AppBundle/Resources/views/formation/listStagiaires.html.twig",array('pagination' =>$formations->getStagiaire(),'theme'=>$theme->getId()));
    }


    public function ajoutFiliereAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $filiere=new Filiere();
        $form = $this->createForm(FiliereForm::class, $filiere);
        $form->handleRequest($request);
        if($request->getMethod()=="POST"){
            $filiere=$form->getData();
            $filiere->setIsDeleted(1);
            $em->persist($filiere);
            $em->flush();
            return $this->redirectToRoute("mfmm_crm_list_filieres");
        }
        return $this->render("@AppBundle/Resources/views/filiere/ajout.html.twig",array('form' => $form->createView()));
    }

    public function ajoutFormationAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $formation=new Formation();
        $form = $this->createForm(FormationForm::class, $formation);
        $form->handleRequest($request);
        if($request->getMethod()=="POST"){
            $formation=$form->getData();
            $formation->setIsDeleted(1);
            $em->persist($formation);
            $em->flush();
            return $this->redirectToRoute("mfmm_crm_list_formations");
        }
        return $this->render("@AppBundle/Resources/views/formation/ajout.html.twig",array('form' => $form->createView()));
    }
    public function ajoutSousFiliereAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $sousfiliere=new SousFiliere();
        $form = $this->createForm(SousFiliereForm::class, $sousfiliere);
        $form->handleRequest($request);
        if($request->getMethod()=="POST"){
            $sousfiliere=$form->getData();
            $sousfiliere->setIsDeleted(1);
            $em->persist($sousfiliere);
            $em->flush();
            return $this->redirectToRoute("mfmm_crm_list_sous_filieres");
        }
        return $this->render("@AppBundle/Resources/views/sousfiliere/ajout.html.twig",array('form' => $form->createView()));
    }


    public function modifierFiliereAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $filiere = $em->getRepository('AppBundle:Filiere')->find($id);
        $form = $this->createForm(FiliereForm::class, $filiere);
        $form->handleRequest($request);
        if($request->getMethod()=="POST"){
            $filiere=$form->getData();
            $em->persist($filiere);
            $em->flush();
            return $this->redirectToRoute("mfmm_crm_list_filieres");
        }
        return $this->render("@AppBundle/Resources/views/filiere/ajout.html.twig",array('form' => $form->createView()));
    }
    public function modifierFormationAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $formation = $em->getRepository('AppBundle:Formation')->find($id);
        $form = $this->createForm(FormationForm::class, $formation);
        $form->handleRequest($request);
        if($request->getMethod()=="POST"){
            $formation=$form->getData();
            $em->persist($formation);
            $em->flush();
            return $this->redirectToRoute("mfmm_crm_list_formations");
        }
        return $this->render("@AppBundle/Resources/views/formation/ajout.html.twig",array('form' => $form->createView()));
    }

    public function modifierSousFiliereAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $sousfiliere = $em->getRepository('AppBundle:SousFiliere')->find($id);
        $form = $this->createForm(SousFiliereForm::class, $sousfiliere);
        $form->handleRequest($request);
        if($request->getMethod()=="POST"){
            $sousfiliere=$form->getData();
            $em->persist($sousfiliere);
            $em->flush();
            return $this->redirectToRoute("mfmm_crm_list_sous_filieres");
        }
        return $this->render("@AppBundle/Resources/views/sousfiliere/ajout.html.twig",array('form' => $form->createView()));
    }


    public function supprimerFiliereAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $filiere = $em->getRepository('AppBundle:Filiere')->find($id);
        $filiere->setIsDeleted(0);
        $em->persist($filiere);
        $em->flush();
        return $this->redirectToRoute("mfmm_crm_list_filieres");

    }

    public function supprimerFormationAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $formation = $em->getRepository('AppBundle:Formation')->find($id);
        $formation->setIsDeleted(0);
        $em->persist($formation);
        $em->flush();
        return $this->redirectToRoute("mfmm_crm_list_formations");

    }

    public function supprimerSousFiliereAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $sousfiliere = $em->getRepository('AppBundle:SousFiliere')->find($id);
        $sousfiliere->setIsDeleted(0);
        $em->persist($sousfiliere);
        $em->flush();
        return $this->redirectToRoute("mfmm_crm_list_sous_filieres");

    }

    public function listClasseAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $classe = $em->getRepository('AppBundle:Classe')->findBy(array('isDeleted'=>1));
        return $this->render("@AppBundle/Resources/views/classe/list.html.twig",array('pagination' => $classe));
    }

    public function ajoutClasseAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $classe=new Classe();
        $form = $this->createForm(ClasseForm::class, $classe);
        $form->handleRequest($request);
        if($request->getMethod()=="POST"){
            $classe=$form->getData();
            $classe->__toString();
            $classe->setCount(0);
            $classe->setIsDeleted(1);
            $em->persist($classe);
            $em->flush();
            return $this->redirectToRoute("mfmm_crm_list_classes");
        }
        return $this->render("@AppBundle/Resources/views/classe/ajout.html.twig",array('form' => $form->createView()));
    }

    public function modifierClasseAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $classe = $em->getRepository('AppBundle:Classe')->find($id);
        $form = $this->createForm(ClasseForm::class, $classe);
        $form->handleRequest($request);
        if($request->getMethod()=="POST"){
            $classe=$form->getData();
            $classe->__toString();
            $em->persist($classe);
            $em->flush();
            return $this->redirectToRoute("mfmm_crm_list_classes");
        }
        return $this->render("@AppBundle/Resources/views/classe/ajout.html.twig",array('form' => $form->createView()));
    }

    public function supprimerClasseAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $classe = $em->getRepository('AppBundle:Classe')->find($id);
        $classe->setIsDeleted(0);
        $em->persist($classe);
        $em->flush();
        return $this->redirectToRoute("mfmm_crm_list_classes");

    }

    public function listMatiereAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $matiere = $em->getRepository('AppBundle:Matiere')->findBy(array('isDeleted'=>1));
        return $this->render("@AppBundle/Resources/views/matiere/list.html.twig",array('pagination' => $matiere));
    }

    public function ajoutMatiereAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $matiere=new Matiere();
        $form = $this->createForm(MatiereForm::class, $matiere);
        $form->handleRequest($request);
        if($request->getMethod()=="POST"){
            $matiere=$form->getData();
            $matiere->setIsDeleted(1);
            $matiere->setNbheure($matiere->getTotalHeure());
            $em->persist($matiere);
            $em->flush();
            return $this->redirectToRoute("mfmm_crm_list_matieres");
        }
        return $this->render("@AppBundle/Resources/views/matiere/ajout.html.twig",array('form' => $form->createView()));
    }

    public function modifierMatiereAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $matiere = $em->getRepository('AppBundle:Matiere')->find($id);
        $form = $this->createForm(MatiereForm::class, $matiere);
        $form->handleRequest($request);
        if($request->getMethod()=="POST"){
            $matiere=$form->getData();
            $matiere->setNbheure($matiere->getTotalHeure());
            $em->persist($matiere);
            $em->flush();
            return $this->redirectToRoute("mfmm_crm_list_matieres");
        }
        return $this->render("@AppBundle/Resources/views/matiere/ajout.html.twig",array('form' => $form->createView()));
    }

    public function supprimerMatiereAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $matiere = $em->getRepository('AppBundle:Matiere')->find($id);
        $matiere->setIsDeleted(0);
        $em->persist($matiere);
        $em->flush();
        return $this->redirectToRoute("mfmm_crm_list_matieres");

    }

    public function listDevoirAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $devoir = $em->getRepository('AppBundle:Devoir')->findBy(array('isDeleted'=>1));
        return $this->render("@AppBundle/Resources/views/devoir/list.html.twig",array('pagination' => $devoir));
    }

    public function ajoutDevoirAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $devoir=new Devoir();
        $annee = $em->getRepository('AppBundle:Annee')->findAll();
        $classe = $em->getRepository('AppBundle:Classe')->findAll();
        $form = $this->createForm(DevoirForm::class, $devoir);
        $form->handleRequest($request);
        if($request->getMethod()=="POST"){
            $devoir=$form->getData();
            $devoir->setIsDeleted(1);
            $em->persist($devoir);
            $em->flush();
            return $this->redirectToRoute("mfmm_crm_list_devoirs");
        }
        return $this->render("@AppBundle/Resources/views/devoir/ajout.html.twig",array('form' => $form->createView(),'annees'=>$annee,'classes'=>$classe));
    }

    public function modifierDevoirAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $devoir = $em->getRepository('AppBundle:Devoir')->find($id);
        $form = $this->createForm(DevoirForm::class, $devoir);
        $form->handleRequest($request);
        if($request->getMethod()=="POST"){
            $devoir=$form->getData();
            $em->persist($devoir);
            $em->flush();
            return $this->redirectToRoute("mfmm_crm_list_devoirs");
        }
        return $this->render("@AppBundle/Resources/views/devoir/ajout.html.twig",array('form' => $form->createView()));
    }

    public function supprimerDevoirAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $devoir = $em->getRepository('AppBundle:Devoir')->find($id);
        $devoir->setIsDeleted(0);
        $em->persist($devoir);
        $em->flush();
        return $this->redirectToRoute("mfmm_crm_list_devoirs");

    }

    public function getClasseFromAnneeAction(Request $request ,$id){
        $em = $this->getDoctrine()->getManager();
        $annee = $em->getRepository('AppBundle:Annee')->find($id);
        $classes = $em->getRepository('AppBundle:Classe')->findBy(array(
            'annee' => $annee
        ));
        $res=array();
        foreach($classes as $classe){
            $res[]=['id'=>$classe->getId(),'nom'=>$classe->getNom()];
        }
        return new JsonResponse($res);
    }

    public function getMatiereFromClasseAction(Request $request ,$id){
        $em = $this->getDoctrine()->getManager();
        $classe = $em->getRepository('AppBundle:Classe')->find($id);
        $matieres = $em->getRepository('AppBundle:Matiere')->findBy(array(
            'classe' => $classe
        ));
        $res=array();
        if(sizeof($matieres)!=0){
            foreach($matieres as $mat){
                $res[]=['id'=>$mat->getId(),'nom'=>$mat->getNom()];
            }
        }else{
            $res[]=['id'=>0,'nom'=>'aucune matière'];
        }
        return new JsonResponse($res);
    }
}
