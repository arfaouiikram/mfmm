<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Absence;
use AppBundle\Entity\Blockage;
use AppBundle\Entity\Emplois;
use AppBundle\Entity\FacturePaiement;
use AppBundle\Entity\Note;
use AppBundle\Entity\Paiement;
use AppBundle\Form\AbsenceForm;
use AppBundle\Form\EmploisForm;
use AppBundle\Form\NoteForm;
use AppBundle\Form\PaiementForm;
use AppBundle\Form\PayForm;
use Doctrine\DBAL\Types\ObjectType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class PaiementController extends Controller
{

    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $paiement=new Paiement();
        $display='none';
        $form = $this->createForm(PaiementForm::class, $paiement);
        $form->handleRequest($request);
        $etudiants=array();
        $pagination=array();
        if($request->getMethod()=='POST'){
            if($form->isValid()){
                $paiement=$form->getData();
                $users = $em->getRepository('AppBundle:User')->findAll();
                $role=$this->container->getParameter('security.role_hierarchy.roles');
                foreach($users as $user) {
                    if ($user->hasRole($role['ROLE_ETUDIANT'][0]) && !($user->isSuperAdmin())) {
                        foreach($user->getUserClasse() as $classe){
                            if($classe==$paiement->getClasse()){
                                $etudiants[]=$user;
                            }
                        }
                    }
                }
                $display='block';
                $pagination=array();
                foreach($etudiants as $etud){
                    $paiements = $em->getRepository('AppBundle:Paiement')->findBy(array('classe'=>$paiement->getClasse(),'user'=>$etud,'isDeleted'=>1));
                    $somme=0;
                    foreach($paiements as $pay){
                        $somme+=$pay->getPrix();
                    }
                    $pagination[]=['etudiant'=>$etud,'somme'=>$somme,'reste'=>$paiement->getClasse()->getPrix()-$somme,'classe'=>$paiement->getClasse()];
                }
                return $this->render("@AppBundle/Resources/views/paiement/list1.html.twig",array('form' =>$form->createView(),'pagination'=>$pagination,'display'=>$display));

            }
        }
        return $this->render("@AppBundle/Resources/views/paiement/list.html.twig",array('form' =>$form->createView(),'pagination'=>$pagination,'display'=>$display));
    }

    public function VoirPaiementAction(Request $request ,$id,$classe){
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->find($id);
        $classe = $em->getRepository('AppBundle:Classe')->find($classe);
        $paiements = $em->getRepository('AppBundle:Paiement')->findBy(array('isDeleted'=>1,'user'=>$user,'classe'=>$classe));
        $res=array();
        $somme=0;
        foreach($paiements as $pay){
            $res[]=['id'=>$pay->getId(),'date'=>$pay->getDate(),'somme'=>$pay->getPrix()];
            $somme=$somme+$pay->getPrix();
        }
        return $this->render("@AppBundle/Resources/views/paiement/listPayEtudiant.html.twig",array('pagination'=>$res,'user'=>$user,'classe'=>$classe,'somme'=>$somme));

    }

    public function ajoutAction(Request $request,$id,$classe){
        if($this->getUser()){
            $paiement=new Paiement();
            $form = $this->createForm(PayForm::class, $paiement);
            $form->handleRequest($request);
            if($request->getMethod()=='POST'){
                if($form->isValid()){
                    $em = $this->getDoctrine()->getManager();
                    $user = $em->getRepository('AppBundle:User')->find($id);
                    $classe = $em->getRepository('AppBundle:Classe')->find($classe);
                    $blockage = $em->getRepository('AppBundle:Blockage')->findBy(array('classe'=>$classe,'user'=>$user));
                    if(sizeof($blockage)!=0){
                        $blockage=$blockage[0];
                        if($classe->getPrix()>$paiement->getPrix()){
                            $blockage->setStatus('bloqué');
                        }else{
                            $blockage->setStatus('non bloqué');
                        }
                    }else{
                        $blockage=new Blockage();
                        $blockage->setUser($user);
                        $blockage->setClasse($classe);
                        $blockage->setIsDeleted(1);
                        if($classe->getPrix()>$paiement->getPrix()){
                            $blockage->setStatus('bloqué');
                        }else{
                            $blockage->setStatus('non bloqué');
                        }
                    }
                    $paiement->setClasse($classe);
                    $paiement->setIsDeleted(1);
                    $paiement->setCreatedBy($this->getUser());
                    $paiement->setUser($user);
                    $em->persist($paiement);
                    $em->persist($blockage);
                    $em->flush();
                    return $this->redirectToRoute('mfmm_crm_voir_Paiement',array('id'=>$user->getId(),'classe'=>$classe->getId()));
                }
            }
            return $this->render("@AppBundle/Resources/views/paiement/ajout.html.twig",array('form'=>$form->createView()));
        }else{
            return $this->redirectToRoute('fos_user_security_login');
        }
    }

    public function modifierAction(Request $request,$id){
        if($this->getUser()){
            $em = $this->getDoctrine()->getManager();
            $paiement = $em->getRepository('AppBundle:Paiement')->find($id);
            $form = $this->createForm(PayForm::class, $paiement);
            $form->handleRequest($request);
            if($request->getMethod()=='POST'){
                if($form->isValid()){
                    $blockage = $em->getRepository('AppBundle:Blockage')->findBy(array('classe'=>$paiement->getClasse(),'user'=>$paiement->getUser()));
                    if(sizeof($blockage)!=0){
                        $blockage=$blockage[0];
                        if($paiement->getClasse()->getPrix()>$paiement->getPrix()){
                            $blockage->setStatus('bloqué');
                        }else{
                            $blockage->setStatus('non bloqué');
                        }
                    }else{
                        $blockage=new Blockage();
                        $blockage->setUser($paiement->getUser());
                        $blockage->setClasse($paiement->getClasse());
                        $blockage->setIsDeleted(1);
                        if($paiement->getClasse()->getPrix()>$paiement->getPrix()){
                            $blockage->setStatus('bloqué');
                        }else{
                            $blockage->setStatus('non bloqué');
                        }
                    }
                    $paiement->setCreatedBy($this->getUser());
                    $em->persist($paiement);
                    $em->persist($blockage);
                    $em->flush();
                    return $this->redirectToRoute('mfmm_crm_voir_Paiement',array('id'=>$paiement->getUser()->getId(),'classe'=>$paiement->getClasse()->getId()));
                }
            }
            return $this->render("@AppBundle/Resources/views/paiement/ajout.html.twig",array('form'=>$form->createView()));
        }else{
            return $this->redirectToRoute('fos_user_security_login');
        }
    }

    public  function supprimerAction(Request $request,$id){
        $em = $this->getDoctrine()->getManager();
        $paiement = $em->getRepository('AppBundle:Paiement')->find($id);
        $paiement->setIsDeleted(0);
        $em->persist($paiement);
        $em->flush();
        return $this->redirectToRoute('mfmm_crm_voir_Paiement',array('id'=>$paiement->getUser()->getId(),'classe'=>$paiement->getClasse()->getId()));
    }

    public function VoirStatusAction(Request $request , $id,$classe){
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->find($id);
        $classe = $em->getRepository('AppBundle:Classe')->find($classe);
        $blockage = $em->getRepository('AppBundle:Blockage')->findBy(array('classe'=>$classe,'user'=>$user));
        if(sizeof($blockage)==0){
            $blockage=null;
        }
        return $this->render("@AppBundle/Resources/views/paiement/blockage.html.twig",array('blockage'=>$blockage?$blockage[0]:null));
    }

    public function changeStatusAction(Request $request ,$id){
        $em = $this->getDoctrine()->getManager();
        $blockage = $em->getRepository('AppBundle:Blockage')->find($id);
        if($blockage->getStatus()=='bloqué'){
            $blockage->setStatus('non bloqué');
        }else{
            $blockage->setStatus('bloqué');
        }
        $em->persist($blockage);
        $em->flush();
        return $this->render("@AppBundle/Resources/views/paiement/blockage.html.twig",array('blockage'=>$blockage));
    }

    public  function importAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $file = $request->files->get('___');
        if($file!=null)
        {
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            // Move the file to the directory where brochures are stored
            $file->move($this->container->getParameter('kernel.root_dir').'/../web/excel',
                $fileName
            );
        }
        $excel = $this->get('os.excel');
        $excel->loadFile($this->container->getParameter('kernel.root_dir').'/../web/excel/'.$fileName);
        $count=  $excel->getRowCount();
        for($i=2;$i<$count;$i++){
            $countC=$excel->getRowData($i);
            if($countC[0]!=null){
                $facture=new FacturePaiement();
                $facture->setTitre($countC[0]);
               // $facture->setDate(new \DateTime($countC[1]));
                $facture->setNom($countC[2]);
                //$facture->setDateE($countC[3]);
                $facture->setPaye($countC[4]);
                $facture->setIsDeleted(1);
                $em->persist($facture);
            }
        }
        $em->flush();
        return $this->redirectToRoute('mfmm_crm_paiements');

    }

    public function exportAction(Request$request){
        $d=$request->request->get('datepicker');
        $date=new \DateTime($d);
        //First sheet
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
        $phpExcelObject->setActiveSheetIndex(0);
        $activesheet = $phpExcelObject->getActiveSheet();
        //Start adding next sheets
        $i=0;
        $em = $this->getDoctrine()->getManager();
        $paiements = $em->getRepository('AppBundle:Paiement')->findBy(array('isDeleted'=>1));
        $pagination=array();
        foreach($paiements as $pay){
            if($pay->getDate()>=$date){
                $pagination[]=$pay;
            }
        }
        $objWorkSheet = $phpExcelObject->createSheet($i); //Setting index when creating
        //Write cells
        $objWorkSheet->setCellValue('A1', 'Nom')
            ->setCellValue('C1', 'Montant')
            ->setCellValue('E1', 'date');
        $i++;
        foreach ($pagination as $pay) {
            $i++;
            //Write cells
            $objWorkSheet->setCellValue('A'.$i, $pay->getUser()->getNom()." ".$pay->getUser()->getPrenom())
                ->setCellValue('C'.$i, $pay->getPrix())
                ->setCellValue('E'.$i, $pay->getDate()?$pay->getDate()->format('Y-m-d'):null);
            $objWorkSheet->setTitle("$i");
        }
        $phpExcelObject->getActiveSheet()->setTitle('Simple');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $phpExcelObject->setActiveSheetIndex(0);

        // create the writer
        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
        // create the response
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        // adding headers
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'stream-file.xls'
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);
        return $response;
    }

    public  function  factureAction(Request $request ){
        $em = $this->getDoctrine()->getManager();
        $factures = $em->getRepository('AppBundle:FacturePaiement')->findAll();
        return $this->render("@AppBundle/Resources/views/paiement/factures.html.twig",array('pagination'=>$factures));
    }


}
