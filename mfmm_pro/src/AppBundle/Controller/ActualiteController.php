<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Absence;
use AppBundle\Entity\Actualite;
use AppBundle\Entity\Emplois;
use AppBundle\Form\AbsenceForm;
use AppBundle\Form\ActualiteForm;
use AppBundle\Form\EmploisForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ActualiteController extends Controller
{

    public function indexAction(Request $request){
        $user=$this->getUser();
        if($user){
            $actualites=array();
            $actualites1=array();
            $em = $this->get('doctrine.orm.entity_manager');
            if($user->hasRole('ROLE_SUPER_ADMIN')){
                $actualites=$em->getRepository('AppBundle:Actualite')->findBy(array('isDeleted'=>1));
            }else{
                $actualites = $em->createQueryBuilder()
                    ->select('u')
                    ->from('AppBundle:Actualite', 'u')
                    ->join('u.actualiteUser','us')
                    ->where('u.isDeleted=1')
                    ->andwhere('us.id='.$user->getId())
                    ->orderBy('u.id','ASC')
                    ->getQuery()
                    ->getResult();
                $classes=$user->getUserClasse();
                foreach($classes as $class){
                    $actualites1 = $em->createQueryBuilder()
                        ->select('u')
                        ->from('AppBundle:Actualite', 'u')
                        ->join('u.classe','c')
                        ->where('u.isDeleted=1')
                        ->andwhere('c.id='.$class->getId())
                        ->orderBy('u.id','ASC')
                        ->getQuery()
                        ->getResult();

                }
            }
            return $this->render("@AppBundle/Resources/views/actualite/list.html.twig",array('pagination'=>$actualites,'pagination1'=>$actualites1));
        }else{
            return $this->redirectToRoute('fos_user_security_login');

        }
    }

    public function ajoutAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $actualite=new Actualite();
        $form = $this->createForm(ActualiteForm::class, $actualite);
        $form->handleRequest($request);
        if($request->getMethod()=='POST'){
            if($form->isValid()){
                $actualite->setIsDeleted(1);
                $em->persist($actualite);
                $em->flush();
                return $this->redirectToRoute('mfmm_crm_actualites');
            }
        }
        return $this->render("@AppBundle/Resources/views/actualite/ajout.html.twig",array('form' =>$form->createView()));
    }

    public function modifierAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $actualite = $em->getRepository('AppBundle:Actualite')->find($id);
        $form = $this->createForm(ActualiteForm::class, $actualite);
        $form->handleRequest($request);
        if($request->getMethod()=='POST'){
            if($form->isValid()){
                $em->persist($actualite);
                $em->flush();
                return $this->redirectToRoute('mfmm_crm_actualites');
            }
        }
        return $this->render("@AppBundle/Resources/views/actualite/ajout.html.twig",array('form' =>$form->createView()));
    }


    public function supprimerAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $actualite = $em->getRepository('AppBundle:Actualite')->find($id);
        $actualite->setIsDeleted(0);
        $em->persist($actualite);
        $em->flush();
        return $this->redirectToRoute('mfmm_crm_actualites');
    }


    public function getTypeAction(Request $request ,$type){
        $em = $this->getDoctrine()->getManager();
        $users = $em->createQueryBuilder()
            ->select('u')
            ->from('AppBundle:User', 'u')
            ->where('u.isDeleted=1')
            ->getQuery()
            ->getResult();
        $role=$this->container->getParameter('security.role_hierarchy.roles');
        $pagination=array();
        if($type=="Etudiant"){
            foreach($users as $user){
                if($user->hasRole($role['ROLE_ETUDIANT'][0]) && !($user->isSuperAdmin())){
                    $pagination[]=['id'=>$user->getId(),'nom'=>$user->getNom().' '.$user->getPrenom()];
                }
            }
        }else{
            foreach($users as $user){
                if($user->hasRole($role['ROLE_ENSEIGNANT'][0]) && !($user->isSuperAdmin())){
                    $pagination[]=['id'=>$user->getId(),'nom'=>$user->getNom().' '.$user->getPrenom()];
                }
            }
        }

        return new JsonResponse($pagination);
    }




}
