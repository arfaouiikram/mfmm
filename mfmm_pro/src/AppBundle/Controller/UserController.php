<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Absence;
use AppBundle\Entity\Annee;
use AppBundle\Entity\AnneeRepository;
use AppBundle\Entity\Classe;
use AppBundle\Entity\ClasseRepository;
use AppBundle\Entity\Devoir;
use AppBundle\Entity\Emplois;
use AppBundle\Entity\Filiere;
use AppBundle\Entity\Groupe;
use AppBundle\Entity\Matiere;
use AppBundle\Entity\Niveau;
use AppBundle\Entity\Note;
use AppBundle\Entity\Semestre;
use AppBundle\Entity\SemestreRepository;
use AppBundle\Form\AbsenceForm;
use AppBundle\Form\AnneeForm;
use AppBundle\Form\ClasseForm;
use AppBundle\Form\DevoirForm;
use AppBundle\Form\EmploisForm;
use AppBundle\Form\FiliereForm;
use AppBundle\Form\GroupeForm;
use AppBundle\Form\MatiereForm;
use AppBundle\Form\NiveauForm;
use AppBundle\Form\SemestreForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\Choice;

class UserController extends Controller
{

    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $users = $em->createQueryBuilder()
            ->select('u')
            ->from('AppBundle:User', 'u')
            ->where('u.isDeleted=1')
            ->getQuery()
            ->getResult();
        $role=$this->container->getParameter('security.role_hierarchy.roles');
        $pagination=array();
        foreach($users as $user){
            if($user->hasRole($role['ROLE_ADMIN'][0]) && !($user->isSuperAdmin())){
                $pagination[]=$user;
            }
        }
        return $this->render("@AppBundle/Resources/views/user/list.html.twig",array('pagination' => $pagination));
    }

    public function ajoutAgentAction(Request $request){
        $roles=$this->container->getParameter('security.role_hierarchy.roles');
        $role=$roles['ROLE_ADMIN'];
        $displayAgent='block';
        $displayEtudiant='none';
        $displayEnseignant='none';
        return $this->redirectToRoute("fos_user_registration_register",array('role'=>$role,'displayAgent'=>$displayAgent,'displayEnseignant'=>$displayEnseignant,'displayEtudiant'=>$displayEtudiant));
    }

    public function modifierAgentAction(Request $request ,$id){
        $displayAgent='block';
        $displayEtudiant='none';
        $displayEnseignant='none';
        return $this->redirectToRoute("fos_user_profile_edit",array('user'=>$id,'displayAgent'=>$displayAgent,'displayEnseignant'=>$displayEnseignant,'displayEtudiant'=>$displayEtudiant));
    }

    public function profileAction(Request $request ,$id){
        if($this->getUser()){
            if($request->getMethod()=='POST'){
                $prenom=$request->request->get('prenom');
                $nom=$request->request->get('nom');
                $pays=$request->request->get('pays');
                $cin=$request->request->get('cin');
                $adresse=$request->request->get('adresse');
                $passe1=$request->request->get('passe1');
                $passe2=$request->request->get('passe2');
                $image=$request->request->get('image');
                $em = $this->get('doctrine.orm.entity_manager');
                $user = $em->getRepository('AppBundle:User')->find($id);
                dump($image);die;
                if($image!=null)
                {
                    $fileName = md5(uniqid()).'.'.$image->guessExtension();
                    // Move the file to the directory where brochures are stored
                    $image->move($this->container->getParameter('kernel.root_dir').'/../web/image/user',
                        $fileName
                    );
                    $user->setPath($fileName);
                    $user->setFile(null);
                }
                if($prenom!=""){
                    $user->setPrenom($prenom);
                }
                if($nom!=""){
                    $user->setNom($nom);
                }
                if($adresse!=""){
                    $user->setAdresse($adresse);
                }
                if($cin!=""){
                    $user->setCin($cin);
                }
                if($pays!=""){
                    $user->setPays($pays);
                }
                /*if($passe1==$passe2 && $passe1!="" && $passe1!=null){
                    $user->setPassword($passe1);
                }
                $userManager = $this->get('fos_user.user_manager');
                $userManager->updateUser($user);*/
                $em->persist($user);
                $em->flush();

                return $this->redirectToRoute('mfmm_crm_homepage');
            }
            return $this->render("@AppBundle/Resources/views/user/profile.html.twig",array('user'=>$this->getUser()));
        }else{
            return $this->redirectToRoute('fos_user_security_login');
        }
    }

    public function supprimerAgentAction(Request $request ,$id){
        $em = $this->get('doctrine.orm.entity_manager');
        $user = $em->getRepository('AppBundle:User')->find($id);
        $user->setIsDeleted(0);
        $em->persist($user);
        $em->flush();
        return $this->redirectToRoute("mfmm_crm_users");

    }

    public function listClasseUserAction(Request $request,$id){
        $em = $this->get('doctrine.orm.entity_manager');
        $user = $em->getRepository('AppBundle:User')->find($id);
        $classes=$user->getUserClasse();
        $tab=array();
        foreach($classes as $classe){
            $tab[]=$classe;
        }
        return $this->render("@AppBundle/Resources/views/user/listClassUser.html.twig",array('pagination' => $tab,'user'=>$user));

    }

    public function listNotesUserAction(Request $request,$id){
        $em = $this->get('doctrine.orm.entity_manager');
        $user = $em->getRepository('AppBundle:User')->find($id);
        $note=new Note();
        $display='none';
        $notes=array();
        $pagination=array();
        $form = $this->createFormBuilder($note)
            ->add('classe', EntityType::class, array(
                'class' => 'AppBundle:Classe',
                'choices' => $user->getUserClasse(),
                'choice_label' => 'nom'))
            ->add('semestre', EntityType::class, array(
                'class' => 'AppBundle:Semestre',
                'query_builder' => function (SemestreRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.nom', 'ASC');
                },
                'choice_label' => 'nom',
                'required' =>true))
            ->getForm();
        $form->handleRequest($request);
        if($request->getMethod()=='POST'){
            if($form->isValid()){
                $note=$form->getData();
                $display='block';
                $notes = $em->getRepository('AppBundle:Note')->findBy(array('classe'=>$note->getClasse(),'user'=>$user));
                $devoirs = $em->getRepository('AppBundle:Devoir')->findBy(array('classe'=>$note->getClasse(),'isDeleted'=>1));
                foreach($devoirs as $dev){
                    $i=0;
                    foreach($notes as $no){
                        if($no->getDevoir()==$dev && $no->getMatiere()->getSemestre()==$note->getSemestre() ){
                            $pagination[]=['devoir'=>$dev,'note'=>$no->getNote()];
                            $i++;
                        }
                    }
                }
                $controle=array();
                $exa=array();
                $notes=array();
                $matieres = $em->getRepository('AppBundle:Matiere')->findBy(array('classe'=>$note->getClasse(),'isDeleted'=>1,'semestre'=>$note->getSemestre()));
                foreach($matieres as $mat){
                    $devoirCc = $em->getRepository('AppBundle:Devoir')->findBy(array('matiere'=>$mat,'isDeleted'=>1,'type'=>'Cc'));
                    $devoirEx = $em->getRepository('AppBundle:Devoir')->findBy(array('matiere'=>$mat,'isDeleted'=>1,'type'=>'Ex'));
                    $noteCc='--';
                    $noteEx='--';
                    if(sizeof($devoirCc)!=0){
                        $note= $em->getRepository('AppBundle:Note')->findBy(array('devoir'=>$devoirCc[0],'user'=>$user));
                        if(sizeof($note)!=0) {
                            $noteCc = $note[0]->getNote();
                        }
                    }
                    if(sizeof($devoirEx)!=0){
                        $note= $em->getRepository('AppBundle:Note')->findBy(array('devoir'=>$devoirEx[0],'user'=>$user));
                        if(sizeof($note)!=0) {
                            $noteEx=$note[0]->getNote();
                        }
                    }
                    $notes[]=['matiere'=>$mat->getNom(),'Cc'=>$noteCc,'Ex'=>$noteEx];
                }

                return $this->render("@AppBundle/Resources/views/user/list1Notes.html.twig",array('form' =>$form->createView(),'pagination'=>$notes,'display'=>$display));
            }
        }

        return $this->render("@AppBundle/Resources/views/user/listNotes.html.twig",array('form' =>$form->createView(),'pagination'=>$pagination,'display'=>$display));

    }

    public function listAbsencesUserAction(Request $request,$id){
        $em = $this->get('doctrine.orm.entity_manager');
        $user = $em->getRepository('AppBundle:User')->find($id);
        $absence=new Absence();
        $display='none';
        $absences=array();
        $pagination=array();
        $form = $this->createFormBuilder($absence)
            ->add('classe', EntityType::class, array(
                'class' => 'AppBundle:Classe',
                'choices' => $user->getUserClasse(),
                'choice_label' => 'nom'))
            ->add('semestre', EntityType::class, array(
                'class' => 'AppBundle:Semestre',
                'query_builder' => function (SemestreRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.nom', 'ASC');
                },
                'choice_label' => 'nom',
                'required' =>true))
            ->getForm();
        $form->handleRequest($request);
        if($request->getMethod()=='POST'){
            if($form->isValid()){
                $absence=$form->getData();
                $display='block';
                $absences = $em->getRepository('AppBundle:Absence')->findBy(array('classe'=>$absence->getClasse(),'semestre'=>$absence->getSemestre(),'user'=>$user));
                $matieres = $em->getRepository('AppBundle:Matiere')->findBy(array('classe'=>$absence->getClasse(),'semestre'=>$absence->getSemestre()));
                foreach($matieres as $matiere){
                    $i=0;
                    foreach($absences as $abs){
                        if($abs->getMatiere()==$matiere){
                            $pagination[]=['matiere'=>$matiere->getNom(),'absences'=>$abs->getNbHeure()];
                            $i++;
                        }
                    }
                    if($i==0){
                        $pagination[]=['matiere'=>$matiere->getNom(),'absences'=>0];
                    }
                }
                return $this->render("@AppBundle/Resources/views/user/list1Absences.html.twig",array('form' =>$form->createView(),'pagination'=>$pagination,'display'=>$display));
            }
        }

        return $this->render("@AppBundle/Resources/views/user/listAbsences.html.twig",array('form' =>$form->createView(),'pagination'=>$pagination,'display'=>$display));
    }

    public function  ajoutClasseAction(Request $request ,$id){
        $em = $this->get('doctrine.orm.entity_manager');
        $user = $em->getRepository('AppBundle:User')->find($id);
        $form = $this->createFormBuilder()
            ->add('annee', EntityType::class, array(
                'class' => 'AppBundle:Annee',
                'query_builder' => function (AnneeRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.nom', 'ASC');
                },
                'choice_label' => 'nom',))
            ->add('classe', EntityType::class, array(
                'class' => 'AppBundle:Classe',
                'query_builder' => function (ClasseRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.nom', 'ASC');
                },
                'choice_label' => 'nom',))
            ->getForm();
            $form->handleRequest($request);
            if($request->getMethod()=='POST'){
                if($form->isValid()){
                    $data=$form->getData();
                    $user->addUserClasse($data['classe']);
                    $em->persist($user);
                    $em->flush();
                    $role=$this->container->getParameter('security.role_hierarchy.roles');
                    if($user->hasRole($role['ROLE_ETUDIANT'][0])){
                        return $this->redirectToRoute("mfmm_crm_modifier_etudiant",array('id'=>$id));
                    }elseif($user->hasRole($role['ROLE_ENSEIGNANT'][0])){
                        return $this->redirectToRoute("mfmm_crm_modifier_professeur",array('id'=>$id));
                    }
                }
            }
        return $this->render("@AppBundle/Resources/views/user/ajoutClasseUser.html.twig",array('form' => $form->createView()));


    }

    public function  supprimerClasseAction(Request $request ,$id,$user){
        $em = $this->get('doctrine.orm.entity_manager');
        $classe = $em->getRepository('AppBundle:Classe')->find($id);
        $user = $em->getRepository('AppBundle:User')->find($user);
        $user->removeUserClasse($classe);
        $em->persist($user);
        $em->flush();
        $role=$this->container->getParameter('security.role_hierarchy.roles');
        if(sizeof($user->getUserClasse())!=0){
            if($user->hasRole($role['ROLE_ETUDIANT'][0])){
                return $this->redirectToRoute("mfmm_crm_modifier_etudiant",array('id'=>$user->getId()));
            }elseif($user->hasRole($role['ROLE_ENSEIGNANT'][0])){
                return $this->redirectToRoute("mfmm_crm_modifier_professeur",array('id'=>$user->getId()));
            }
        }else{
            if($user->hasRole($role['ROLE_ETUDIANT'][0])){
                return $this->redirectToRoute("mfmm_crm_annee_to_etudiants");
            }elseif($user->hasRole($role['ROLE_ENSEIGNANT'][0])){
                return $this->redirectToRoute("mfmm_crm_professeurs");
            }
        }


    }




}
