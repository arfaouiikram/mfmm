<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Absence;
use AppBundle\Entity\Emplois;
use AppBundle\Entity\Note;
use AppBundle\Form\AbsenceForm;
use AppBundle\Form\EmploisForm;
use AppBundle\Form\NoteForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class NoteController extends Controller
{

    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $note=new Note();
        $display='none';
        $form = $this->createForm(NoteForm::class, $note);
        $form->handleRequest($request);
        $etudiants=array();
        $pagination=array();
        if($request->getMethod()=='POST'){
            if($form->isValid()){
                $note=$form->getData();
                $users = $em->getRepository('AppBundle:User')->findAll();
                $role=$this->container->getParameter('security.role_hierarchy.roles');
                foreach($users as $user) {
                    if ($user->hasRole($role['ROLE_ETUDIANT'][0]) && !($user->isSuperAdmin())) {
                        foreach($user->getUserClasse() as $classe){
                            if($classe==$note->getClasse()){
                                $etudiants[]=$user;
                            }
                        }
                    }
                }
                $display='block';
                $notes = $em->getRepository('AppBundle:Note')->findBy(array('classe'=>$note->getClasse(),'matiere'=>$note->getMatiere(),'devoir'=>$note->getDevoir()));
                $pagination=array();
                foreach($etudiants as $etud){
                    $i=0;
                    foreach($notes as $no){
                        if($no->getUser()==$etud){
                            $pagination[]=['etudiant'=>$etud,'note'=>$no->getNote()];
                            $i++;
                        }
                    }
                    if($i==0){
                        $pagination[]=['etudiant'=>$etud,'note'=>0];
                    }
                }
                return $this->render("@AppBundle/Resources/views/note/list1.html.twig",array('form' =>$form->createView(),'pagination'=>$pagination,'display'=>$display));

            }
        }
        return $this->render("@AppBundle/Resources/views/note/list.html.twig",array('form' =>$form->createView(),'pagination'=>$pagination,'display'=>$display));
    }

    public function getDevoirAction(Request $request ,$matiere){
        $em = $this->getDoctrine()->getManager();
        $mat = $em->getRepository('AppBundle:Matiere')->find($matiere);
        $devoirs = $em->getRepository('AppBundle:Devoir')->findBy(array('isDeleted'=>1,'matiere'=>$mat));
        $res=array();
        foreach($devoirs as $dev){
            $res[]=['id'=>$dev->getId(),'nom'=>$dev->getTYpe(). '   ||     ' . $dev->getNom()];
        }
        return new JsonResponse($res);
    }

    public function ajoutNotesDevoirAction(Request $request,$ligne,$classe,$matiere,$devoir){
        $col=split(',',$ligne);
        $tab=array();
        $em = $this->getDoctrine()->getManager();
        $classe = $em->getRepository('AppBundle:Classe')->find($classe);
        $devoir = $em->getRepository('AppBundle:Devoir')->find($devoir);
        $matiere = $em->getRepository('AppBundle:Matiere')->find($matiere);
        foreach($col as $c){
            if($c!=""){
                $ab=split(':',$c);
                $tab[]=['etudiant'=>$ab[0],'note'=>$ab[1]];
            }
        }
        $notes = $em->getRepository('AppBundle:Note')->findBy(array('classe'=>$classe,'matiere'=>$matiere,'devoir'=>$devoir));
        foreach($notes as $no){
            $em->remove($no);
            $em->flush();
        }
        foreach($tab as $t){
            $note=new Note();
            $user = $em->getRepository('AppBundle:User')->find($t['etudiant']);
            $note->setClasse($classe);
            $note->setMatiere($matiere);
            $note->setDevoir($devoir);
            $note->setUser($user);
            $note->setNote($t['note']);
            $note->setIsDeleted(1);
            $em->persist($note);
        }
        $em->flush();
        return $this->redirectToRoute("mfmm_crm_notes");
    }


}
