<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Annee;
use AppBundle\Entity\Classe;
use AppBundle\Entity\Devoir;
use AppBundle\Entity\Emplois;
use AppBundle\Entity\Filiere;
use AppBundle\Entity\Groupe;
use AppBundle\Entity\Matiere;
use AppBundle\Entity\Niveau;
use AppBundle\Entity\Semestre;
use AppBundle\Form\AnneeForm;
use AppBundle\Form\ClasseForm;
use AppBundle\Form\DevoirForm;
use AppBundle\Form\EmploisForm;
use AppBundle\Form\FiliereForm;
use AppBundle\Form\GroupeForm;
use AppBundle\Form\MatiereForm;
use AppBundle\Form\NiveauForm;
use AppBundle\Form\SemestreForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EtudiantController extends Controller
{

    public function indexAction(Request $request,$annee)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $results=array();
        $users = $em->createQueryBuilder()
            ->select('u')
            ->from('AppBundle:User','u')
            ->where('u.isDeleted=1')
            ->getQuery()
            ->getResult();
        $role=$this->container->getParameter('security.role_hierarchy.roles');
            $pagination=array();
            foreach($users as $user){
                if($user->hasRole($role['ROLE_ETUDIANT'][0]) && !($user->isSuperAdmin())){
                   // foreach($user->getUserClasse() as $sfiliere){
                    if($user->getAnnee()){
                        if($user->getAnnee()->getId()==$annee){
                            $pagination[]=['user'=>$user];
                        }
                    }
                }
        }
        return $this->render("@AppBundle/Resources/views/etudiant/list.html.twig",array('pagination' => $pagination));
    }

    public function anneeEtudiantAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $emplois = $em->createQueryBuilder()
            ->select('e')
            ->from('AppBundle:Annee', 'e')
            ->getQuery()
            ->getResult();
        return $this->render("@AppBundle/Resources/views/etudiant/anneeToSelect.html.twig",array('annees' => $emplois));
    }

    public function ajoutEtudiantAction(Request $request){
        $roles=$this->container->getParameter('security.role_hierarchy.roles');
        $role=$roles['ROLE_ETUDIANT'];
        $displayAgent='none';
        $displayEtudiant='block';
        $displayEnseignant='none';
        return $this->redirectToRoute("fos_user_registration_register",array('role'=>$role,'displayAgent'=>$displayAgent,'displayEnseignant'=>$displayEnseignant,'displayEtudiant'=>$displayEtudiant));
    }

    public function modifierEtudiantAction(Request $request ,$id){
        $displayAgent='none';
        $displayEtudiant='block';
        $displayEnseignant='none';
        return $this->redirectToRoute("fos_user_profile_edit",array('user'=>$id,'displayAgent'=>$displayAgent,'displayEnseignant'=>$displayEnseignant,'displayEtudiant'=>$displayEtudiant));
    }
    public function supprimerEtudiantAction(Request $request ,$id){
        $em = $this->get('doctrine.orm.entity_manager');
        $user = $em->getRepository('AppBundle:User')->find($id);
        $user->setIsDeleted(0);
        $em->persist($user);
        $em->flush();
        return $this->redirectToRoute("mfmm_crm_annee_to_etudiants");

    }

    public function getClasseAction(Request $request ,$annee){
        $em = $this->getDoctrine()->getManager();
        $an = $em->getRepository('AppBundle:Annee')->find($annee);
        $classes = $em->getRepository('AppBundle:Filiere')->findBy(array(
            'annee' => $an,
            'isDeleted'=>1
        ));
        $res=array();
        foreach($classes as $classe){
            $res[]=['id'=>$classe->getId(),'nom'=>$classe->getNom()];
        }
        return new JsonResponse($res);
    }

    public function getSousFiliereAction(Request $request ,$filiere){
        $em = $this->getDoctrine()->getManager();
        $filiere = $em->getRepository('AppBundle:Filiere')->find($filiere);
        $sousFiliere = $em->getRepository('AppBundle:SousFiliere')->findAll();
        $classes=array();
        foreach($sousFiliere as $cl){
            $filieres=$cl->getSousFilereFiliere();
            foreach($filieres as $fi){
                if($fi==$filiere){
                    $classes[]=$cl;
                }
            }
        }
        $res=array();
        foreach($classes as $classe){
            $res[]=['id'=>$classe->getId(),'nom'=>$classe->getNom()];
        }
        return new JsonResponse($res);
    }

    public function getFormationAction(Request $request ,$sfiliere){
        $em = $this->getDoctrine()->getManager();
        $sfiliere = $em->getRepository('AppBundle:SousFiliere')->find($sfiliere);
        $classes = $em->getRepository('AppBundle:Formation')->findBy(array(
            'sfiliere' => $sfiliere,
            'isDeleted'=>1
        ));
        $res=array();
        foreach($classes as $classe){
            $res[]=['id'=>$classe->getId(),'nom'=>$classe->getNom()];
        }
        return new JsonResponse($res);
    }


    public function carteEtudiantAction(Request $request,$id,$annee,$classe)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $user = $em->getRepository('AppBundle:User')->find($id);
        $annee = $em->getRepository('AppBundle:Annee')->find($annee);
        $classe = $em->getRepository('AppBundle:Classe')->find($classe);
        $template = 'carte.html.twig';
        $filename = sprintf('convention-%s.pdf', $user->getId());
        $html = $this->renderView($template,array('user'=>$user,'annee'=>$annee,'classe'=>$classe));
        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html)
            ,
            200,
            [
                'Content-Type'        => 'application/pdf',
                'Content-Disposition' => sprintf('inline; filename="%s"', $filename),
            ]
        );
    }

    public function bulletinAction(Request $request,$id,$annee,$classe)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $classe = $em->getRepository('AppBundle:Classe')->find($classe);
        $annee = $em->getRepository('AppBundle:Annee')->find($annee);
        $user = $em->getRepository('AppBundle:User')->find($id);
        $semestres = $em->getRepository('AppBundle:Semestre')->findAll();
        $notes = $em->getRepository('AppBundle:Note')->findBy(array('classe'=>$classe,'user'=>$user));
        $devoirs = $em->getRepository('AppBundle:Devoir')->findBy(array('classe'=>$classe,'isDeleted'=>1));
        $res=array();
        foreach($semestres as $semestre){
            $pagination=array();
            $moyGen=0;
            $matieres = $em->getRepository('AppBundle:Matiere')->findBy(array('classe'=>$classe,'isDeleted'=>1,'semestre'=>$semestre));
            $resMatGlobel=array();
            $coif=0;
            $coifGen=0;
            foreach($matieres as $mat){
                $resMat=array();
                $moyenne=0;
                $i=0;
                $j=0;
                foreach($notes as $no){
                    if($no->getMatiere()==$mat ){
                        if($no->getDevoir()->getType()=='Ex'){
                            $resMat['Ex']=$no->getNote();
                            $moyenne+=$no->getNote()*2;
                            $i++;
                        }else{
                            $resMat['Cc']=$no->getNote();
                            $moyenne+=$no->getNote();
                            $j++;
                        }
                        $coif=$no->getDevoir()->getCoif();
                        $coifGen+=$no->getDevoir()->getCoif();

                    }
                }
                if($i==0){
                    $resMat['Ex']=0;
                }
                if($j==0){
                    $resMat['Cc']=0;
                }
                $resMat['Moy']=$moyenne/3;
                $moyGen+=($moyenne/3)*$coif;
                $resMatGlobel[]=['Matiere'=>$mat->getNom(),'notes'=>$resMat];
            }
            if($coifGen==0){
                $coifGen=1;
            }
            $res[]=['semestre'=>$semestre->getNom(),'dev'=>$resMatGlobel,'moyenne'=>$moyGen/($coifGen)];

        }
        return $this->render("@AppBundle/Resources/views/etudiant/bulletin.html.twig",array('pagination'=>$res));

    }

    public  function ficheAction(Request $request ,$id,$formation){
        $em = $this->get('doctrine.orm.entity_manager');
        $etudiant = $em->getRepository('AppBundle:User')->find($id);
        $formation = $em->getRepository('AppBundle:SousFiliere')->find($formation);
        $template = 'fiche.html.twig';
        $filename = sprintf('fiche-%s.pdf', $etudiant->getId());
        $html = $this->renderView($template,array('user'=>$etudiant,'formation'=>$formation));
        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html)
            ,
            200,
            [
                'Content-Type'        => 'application/pdf',
                'Content-Disposition' => sprintf('inline; filename="%s"', $filename),
            ]
        );
    }




}
