<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Absence;
use AppBundle\Entity\Actualite;
use AppBundle\Entity\Emplois;
use AppBundle\Entity\UserRepository;
use AppBundle\Form\AbsenceForm;
use AppBundle\Form\ActualiteForm;
use AppBundle\Form\EmploisForm;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AttestationController extends Controller
{

    public function indexAction(Request $request){
        $form = $this->createFormBuilder()
            ->add('formation', ChoiceType::class, array(
                'choices'  => array(
                    'AFT' => 1,
                    'AOT' => 2,
                    'BOCO' => 3,
                    'BOCR' => 5,
                    'BRM' => 6,
                    'BT' => 7,
                    'CMT' => 8,
                    'ECDIS' => 9,
                    'ERM' => 10,
                    'FRB' => 11,
                    'GOC'=>4,
                    'HBT' => 12,
                    'PETROLE' => 13,
                    'PETROLE AVANCÉ' => 14,
                    'PSC' => 15,
                    'RADAR' => 16,
                    'ROC' => 17,
                    'SDSD' => 18,
                    'SMU' => 19,
                    'SRB' => 20,
                    'SSA' => 21,
                    'SSO' => 22,
                    'ST' => 23,
                    //'FCMT' => 24,
                    //'CNGD' => 25,
                    //'GRP' => 26,
                    //'GS' => 28,
                    //'IAIA' => 29,
                    //'IMDG' => 30,
                    //'PFSO' => 31,
                    //'PETROLE BASIC' => 35,
                    //'TMD' => 36,
                    //'MARITIME' => 37,
                ),
            ))
            ->add('Stagiaire', EntityType::class, array(
                'class' => 'AppBundle:User',
                'query_builder' => function (UserRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->andWhere('u.roles LIKE :role')
                        ->orderBy('u.nom', 'ASC')
                        ->setParameter('role','%"ROLE_ETUDIANT"%');
                },
                'choice_label' => 'nom'))
            ->getForm();
        $form->handleRequest($request);
        if($request->getMethod()=='POST'){
               $data=$form->getData();
               if($data['formation']==1) {
                   $template = 'AFT.html.twig';
                   $filename = sprintf('AFT -%s.pdf', $data['Stagiaire']->getId());
               }elseif($data['formation']==2){
                   $template = 'AOT.html.twig';
                   $filename = sprintf('AOT -%s.pdf', $data['Stagiaire']->getId());
               }elseif($data['formation']==3){
                   $template = 'BOCO.html.twig';
                   $filename = sprintf('BOCO -%s.pdf', $data['Stagiaire']->getId());
               }elseif($data['formation']==5){
                   $template = 'BOCR.html.twig';
                   $filename = sprintf('BOCR -%s.pdf', $data['Stagiaire']->getId());
               }elseif($data['formation']==7){
                   $template = 'BT.html.twig';
                   $filename = sprintf('BT -%s.pdf', $data['Stagiaire']->getId());
               }elseif($data['formation']==8){
                   $template = 'CMT.html.twig';
                   $filename = sprintf('CMT -%s.pdf', $data['Stagiaire']->getId());
               }elseif($data['formation']==11){
                   $template = 'FRB.html.twig';
                   $filename = sprintf('FRB -%s.pdf', $data['Stagiaire']->getId());
               }elseif($data['formation']==4){
                   $template = 'GOC.html.twig';
                   $filename = sprintf('GOC -%s.pdf', $data['Stagiaire']->getId());
               }elseif($data['formation']==12){
                   $template = 'HBT.html.twig';
                   $filename = sprintf('HBT -%s.pdf', $data['Stagiaire']->getId());
               }elseif($data['formation']==15){
                   $template = 'PSC.html.twig';
                   $filename = sprintf('PSC -%s.pdf', $data['Stagiaire']->getId());
               }elseif($data['formation']==16){
                   $template = 'RADAR.html.twig';
                   $filename = sprintf('RADAR -%s.pdf', $data['Stagiaire']->getId());
               }elseif($data['formation']==17){
                   $template = 'ROC.html.twig';
                   $filename = sprintf('ROC -%s.pdf', $data['Stagiaire']->getId());
               }elseif($data['formation']==18){
                   $template = 'SDSD.html.twig';
                   $filename = sprintf('SDSD -%s.pdf', $data['Stagiaire']->getId());
               }elseif($data['formation']==19){
                   $template = 'SMU.html.twig';
                   $filename = sprintf('SMU -%s.pdf', $data['Stagiaire']->getId());
               }elseif($data['formation']==20){
                   $template = 'SRB.html.twig';
                   $filename = sprintf('SRB -%s.pdf', $data['Stagiaire']->getId());
               }elseif($data['formation']==21){
                   $template = 'SSA.html.twig';
                   $filename = sprintf('SSA -%s.pdf', $data['Stagiaire']->getId());
               }elseif($data['formation']==22){
                   $template = 'SSO.html.twig';
                   $filename = sprintf('SSO -%s.pdf', $data['Stagiaire']->getId());
               }elseif($data['formation']==23){
                   $template = 'ST.html.twig';
                   $filename = sprintf('ST -%s.pdf', $data['Stagiaire']->getId());
               }elseif($data['formation']==6){
                   $template = 'BRM.html.twig';
                   $filename = sprintf('BRM -%s.pdf', $data['Stagiaire']->getId());
               }elseif($data['formation']==9){
                   $template = 'ECDIS.html.twig';
                   $filename = sprintf('ECDIS -%s.pdf', $data['Stagiaire']->getId());
               }elseif($data['formation']==10){
                   $template = 'ERM.html.twig';
                   $filename = sprintf('ERM -%s.pdf', $data['Stagiaire']->getId());
               }elseif($data['formation']==13){
                   $template = 'PETROLE.html.twig';
                   $filename = sprintf('PETROLE -%s.pdf', $data['Stagiaire']->getId());
               }elseif($data['formation']==14){
                   $template = 'PETROLEA.html.twig';
                   $filename = sprintf('PETROLEA -%s.pdf', $data['Stagiaire']->getId());
               }
               $html = $this->renderView($template,array('user'=>$data['Stagiaire']));
               return new Response(
                   $this->get('knp_snappy.pdf')->getOutputFromHtml($html, array(
                       'orientation' => 'Landscape',
                   ))
                   ,
                   200,
                   [
                       'Content-Type'        => 'application/pdf',
                       'Content-Disposition' => sprintf('inline; filename="%s"', $filename),
                   ]
               );
        }
        return $this->render("@AppBundle/Resources/views/attestation/list.html.twig",array('form' =>$form->createView()));

    }

    public function ajoutAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $actualite=new Actualite();
        $form = $this->createForm(ActualiteForm::class, $actualite);
        $form->handleRequest($request);
        if($request->getMethod()=='POST'){
            if($form->isValid()){
                $actualite->setIsDeleted(1);
                $em->persist($actualite);
                $em->flush();
                return $this->redirectToRoute('mfmm_crm_actualites');
            }
        }
        return $this->render("@AppBundle/Resources/views/actualite/ajout.html.twig",array('form' =>$form->createView()));
    }

    public function modifierAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $actualite = $em->getRepository('AppBundle:Actualite')->find($id);
        $form = $this->createForm(ActualiteForm::class, $actualite);
        $form->handleRequest($request);
        if($request->getMethod()=='POST'){
            if($form->isValid()){
                $em->persist($actualite);
                $em->flush();
                return $this->redirectToRoute('mfmm_crm_actualites');
            }
        }
        return $this->render("@AppBundle/Resources/views/actualite/ajout.html.twig",array('form' =>$form->createView()));
    }

    public function supprimerAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $actualite = $em->getRepository('AppBundle:Actualite')->find($id);
        $actualite->setIsDeleted(0);
        $em->persist($actualite);
        $em->flush();
        return $this->redirectToRoute('mfmm_crm_actualites');
    }

    }
