<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        if($this->getUser()){
            $em = $this->get('doctrine.orm.entity_manager');
            $filieres = $em->getRepository('AppBundle:Filiere')->findBy(array('isDeleted'=>1));
            $sousF = $em->getRepository('AppBundle:SousFiliere')->findBy(array('isDeleted'=>1));
            $users = $em->getRepository('AppBundle:User')->findAll();
            $results=array();
            foreach($filieres as $filere){
                $pagination=array();
                foreach($sousF as $sousf){
                    $fil=$sousf->getSousFilereFiliere();
                    foreach($fil as $fi){
                        $formation = $em->getRepository('AppBundle:Formation')->findBy(array('sfiliere'=>$sousf,'isDeleted'=>1));
                        if($fi==$filere){
                            $count=0;
                            foreach($users as $user){
                                $sfs=$user->getUserClasse();
                                foreach($sfs as $f){
                                    if($f==$sousf){
                                        $count++;
                                    }
                                }
                            }
                            $pagination[]=['sousFiliere'=>$sousf,'count'=>count($formation)];
                        }
                    }
                }
                $results[]=['filiere'=>$filere,'stat'=>$pagination];
            }
            return $this->render('default/index.html.twig',array('results'=>$results));
        }else{
            return $this->redirectToRoute('fos_user_security_login');
        }

    }
}
