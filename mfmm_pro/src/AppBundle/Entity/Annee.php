<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Annee
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\AnneeRepository")
 */

class Annee
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="nom", type="string" , length=255 , nullable=true)
     */
    private $nom;

    /**
     * @var string
     * @ORM\Column(name="code", type="string" , length=255 , nullable=true)
     */
    private $code;

    /**
     * @var date
     * @ORM\Column(name="dateD", type="date" , length=255 , nullable=true)
     */
    private $dateD;

    /**
     * @var date
     * @ORM\Column(name="dateF", type="date" , length=255 , nullable=true)
     */
    private $dateF;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isDeleted", type="boolean", nullable=true)
     */
    private $isDeleted;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param boolean $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
    }


    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return date
     */
    public function getDateD()
    {
        return $this->dateD;
    }

    /**
     * @param string $dateD
     */
    public function setDateD($dateD)
    {
        $this->dateD = $dateD;
    }

    /**
     * @return date
     */
    public function getDateF()
    {
        return $this->dateF;
    }

    /**
     * @param string $dateF
     */
    public function setDateF($dateF)
    {
        $this->dateF = $dateF;
    }

    public function __toString(){
        return $this->dateD->format("Y")."/".$this->dateF->format("Y");
    }

}
