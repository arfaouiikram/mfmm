<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Formation
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\FormationRepository")
 */

class Formation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="nom", type="string" , length=255 , nullable=true)
     */
    private $nom;


    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SousFiliere", inversedBy="User"))
     * @ORM\JoinColumn(name="sfiliere", referencedColumnName="id" , nullable=true)
     */
    private $sfiliere;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Salle", inversedBy="User"))
     * @ORM\JoinColumn(name="salle", referencedColumnName="id" , nullable=true)
     */
    private $salle;



    /**
     * @var date
     * @ORM\Column(name="dateD", type="date" , length=255 , nullable=true)
     */
    private $dateD;

    /**
     * @var date
     * @ORM\Column(name="dateF", type="date" , length=255 , nullable=true)
     */
    private $dateF;

    /**
     * @var string
     * @ORM\Column(name="lieu", type="string" , length=255 , nullable=true)
     */
    private $lieu;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinTable(name="Formation_formateur",
     *      joinColumns={@ORM\JoinColumn(name="formation", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user", referencedColumnName="id", unique=false)}
     *      )
     */
    private $Formateur;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinTable(name="Formation_stagiares",
     *      joinColumns={@ORM\JoinColumn(name="formation", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="stagiarie", referencedColumnName="id", unique=false)}
     *      )
     */
    private $Stagiaire;



    /**
     * @return mixed
     */
    public function getStagiaire()
    {
        return $this->Stagiaire;
    }

    /**
     * @param mixed $Stagiaire
     */
    public function setStagiaire($Stagiaire)
    {
        $this->Stagiaire = $Stagiaire;
    }

    public function addStagiaire(\AppBundle\Entity\User $Stagiaire)
    {
        $this->Stagiaire->add($Stagiaire);
    }

    public function removeStagiaire(\AppBundle\Entity\User $Stagiaire)
    {
        $this->Stagiaire->removeElement($Stagiaire);
    }

    public function __construct()
    {
        $this->Formateur = new \Doctrine\Common\Collections\ArrayCollection();
        $this->Stagiaire = new \Doctrine\Common\Collections\ArrayCollection();

    }

    /**
     * @return mixed
     */
    public function getFormateur()
    {
        return $this->Formateur;
    }

    /**
     * @param mixed $Formateur
     */
    public function setFormateur($Formateur)
    {
        $this->Formateur = $Formateur;
    }

    public function addFormateur(\AppBundle\Entity\User $Formateur)
    {
        $this->Formateur->add($Formateur);
    }

    public function removeFormateur(\AppBundle\Entity\User $Formateur)
    {
        $this->Formateur->removeElement($Formateur);
    }

    /**
     * @return date
     */
    public function getDateD()
    {
        return $this->dateD;
    }

    /**
     * @param string $dateD
     */
    public function setDateD($dateD)
    {
        $this->dateD = $dateD;
    }

    /**
     * @return date
     */
    public function getDateF()
    {
        return $this->dateF;
    }

    /**
     * @param string $dateF
     */
    public function setDateF($dateF)
    {
        $this->dateF = $dateF;
    }





    /**
     * @var boolean
     *
     * @ORM\Column(name="isDeleted", type="boolean", nullable=true)
     */
    private $isDeleted;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSfiliere()
    {
        return $this->sfiliere;
    }

    /**
     * @param mixed $sfiliere
     */
    public function setSfiliere($sfiliere)
    {
        $this->sfiliere = $sfiliere;
    }

    /**
     * @return mixed
     */
    public function getSalle()
    {
        return $this->salle;
    }

    /**
     * @param mixed salle
     */
    public function setSalle($salle)
    {
        $this->salle = $salle;
    }

    /**
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param boolean $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
    }


    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return string
     */
    public function getLieu()
    {
        return $this->lieu;
    }

    /**
     * @param string $lieu
     */
    public function setLieu($lieu)
    {
        $this->lieu = $lieu;
    }




}
