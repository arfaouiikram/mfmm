<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Filiere
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\FiliereRepository")
 */

class Filiere
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="nom", type="string" , length=255 , nullable=true)
     */
    private $nom;

    /**
     * @var string
     * @ORM\Column(name="nomA", type="string" , length=255 , nullable=true)
     */
    private $nomA;

    /**
     * @var string
     * @ORM\Column(name="nomE", type="string" , length=255 , nullable=true)
     */
    private $nomE;

    /**
     * @var string
     * @ORM\Column(name="code", type="string" , length=255 , nullable=true)
     */
    private $code;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Annee", inversedBy="User"))
     * @ORM\JoinColumn(name="annee", referencedColumnName="id" , nullable=true)
     */
    private $annee;



    /**
     * @var boolean
     *
     * @ORM\Column(name="isDeleted", type="boolean", nullable=true)
     */
    private $isDeleted;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * @param mixed annee
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;
    }



    /**
     * @return string
     */
    public function getNomE()
    {
        return $this->nomE;
    }

    /**
     * @param string $nomE
     */
    public function setNomE($nomE)
    {
        $this->nomE = $nomE;
    }


    /**
     * @return string
     */
    public function getNomA()
    {
        return $this->nomA;
    }

    /**
     * @param string $nomA
     */
    public function setNomA($nomA)
    {
        $this->nomA = $nomA;
    }

    /**
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param boolean $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
    }


    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }


    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

}
