<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

    /**
 * User
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="nom", type="string" , length=255 , nullable=true)
     */
    private $nom;

    /**
     * @var string
     * @ORM\Column(name="nomA", type="string" , length=255 , nullable=true)
     */
    private $nomA;

    /**
     * @var string
     * @ORM\Column(name="nomE", type="string" , length=255 , nullable=true)
     */
    private $nomE;

    /**
     * @var string
     * @ORM\Column(name="prenom", type="string" , length=255 , nullable=true)
     */
    private $prenom;

    /**
     * @var string
     * @ORM\Column(name="prenomA", type="string" , length=255 , nullable=true)
     */
    private $prenomA;

    /**
     * @var string
     * @ORM\Column(name="prenomE", type="string" , length=255 , nullable=true)
     */
    private $prenomE;

    /**
     * @var string
     * @ORM\Column(name="poste", type="string" , length=255 , nullable=true)
     */
    private $poste;

    /**
     * @var string
     * @ORM\Column(name="telp", type="string" , length=255 , nullable=true)
     */
    private $telp;

    /**
     * @var string
     * @ORM\Column(name="teld", type="string" , length=255 , nullable=true)
     */
    private $teld;

    /**
     * @var string
     * @ORM\Column(name="employeur", type="string" , length=255 , nullable=true)
     */
    private $employeur;



    /**
     * @var string
     * @ORM\Column(name="adresse", type="string" , length=255 , nullable=true)
     */
    private $adresse;


    /**
     * @var string
     * @ORM\Column(name="pays", type="string" , length=255 , nullable=true)
     */
    private $pays;

    /**
     * @var string
     * @ORM\Column(name="ville", type="string" , length=255 , nullable=true)
     */
    private $ville;

    /**
     * @var string
     * @ORM\Column(name="codepostale", type="string" , length=255 , nullable=true)
     */
    private $codepostale;

    /**
     * @var string
     * @ORM\Column(name="cin", type="string" , length=255 , nullable=true)
     */
    private $cin;

    /**
     * @var string
     * @ORM\Column(name="lieuC", type="string" , length=255 , nullable=true)
     */
    private $lieuC;

    /**
     * @var string
     * @ORM\Column(name="code", type="string" , length=255 , nullable=true)
     */
    private $code;

    /**
     * @var string
     * @ORM\Column(name="sexe", type="string" , length=255 , nullable=true)
     */
    private $sexe;

    /**
     * @var string
     * @ORM\Column(name="niveau", type="string" , length=255 , nullable=true)
     */
    private $niveau;

    /**
     * @var float
     * @ORM\Column(name="salaire", type="float" , nullable=true)
     */
    private $salaire;

    /**
     * @var string
     * @ORM\Column(name="lieunaissance", type="string" , length=255 , nullable=true)
     */
    private $lieunaissance;

    /**
     * @var date
     * @ORM\Column(name="datenaissance", type="date" , length=255 , nullable=true)
     */
    private $datenaissance;

    /**
     * @var date
     * @ORM\Column(name="dateC", type="date" , length=255 , nullable=true)
     */
    private $dateC;


    /**
     * @var string
     * @ORM\Column(name="path", type="string" , length=255 , nullable=true)
     */
    private $path;


    private $file;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isDeleted", type="boolean", nullable=true)
     */
    private $isDeleted;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\SousFiliere")
     * @ORM\JoinTable(name="user_sous_filiere",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="sous_filiere_id", referencedColumnName="id", unique=false)}
     *      )
     */
    private $userClasse;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SousFiliere", inversedBy="User"))
     * @ORM\JoinColumn(name="sfiliere", referencedColumnName="id" , nullable=true)
     */
    private $sfiliere;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Filiere", inversedBy="User"))
     * @ORM\JoinColumn(name="filiere", referencedColumnName="id" , nullable=true)
     */
    private $filiere;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Annee", inversedBy="User"))
     * @ORM\JoinColumn(name="annee", referencedColumnName="id" , nullable=true)
     */
    private $annee;






    /**
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param boolean $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
    }


    /**
     * @return mixed
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * @param mixed annee
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;
    }


    /**
     * @return mixed
     */
    public function getFiliere()
    {
        return $this->filiere;
    }

    /**
     * @param mixed $filiere
     */
    public function setFiliere($filiere)
    {
        $this->filiere = $filiere;
    }

    /**
     * @return mixed
     */
    public function getSfiliere()
    {
        return $this->sfiliere;
    }

    /**
     * @param mixed $sfiliere
     */
    public function setSfiliere($sfiliere)
    {
        $this->sfiliere = $sfiliere;
    }


    public function __construct()
    {
        parent::__construct();
        $this->userClasse = new \Doctrine\Common\Collections\ArrayCollection();
        $this->SousFilereFiliere = new \Doctrine\Common\Collections\ArrayCollection();

        // your own logic
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return float
     */
    public function getSalaire()
    {
        return $this->salaire;
    }

    /**
     * @param string $salaire
     */
    public function setSalaire($salaire)
    {
        $this->salaire = $salaire;
    }

    /**
     * @return string
     */
    public function getNomA()
    {
        return $this->nomA;
    }

    /**
     * @param string $nomA
     */
    public function setNomA($nomA)
    {
        $this->nomA = $nomA;
    }

    /**
     * @return string
     */
    public function getNomE()
    {
        return $this->nomE;
    }

    /**
     * @param string $nomE
     */
    public function setNomE($nomE)
    {
        $this->nomE = $nomE;
    }

    /**
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param string $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * @return string
     */
    public function getPrenomA()
    {
        return $this->prenomA;
    }

    /**
     * @param string $prenomA
     */
    public function setPrenomA($prenomA)
    {
        $this->prenomA = $prenomA;
    }

    /**
     * @return string
     */
    public function getPrenomE()
    {
        return $this->prenomE;
    }

    /**
     * @param string $prenomE
     */
    public function setPrenomE($prenomE)
    {
        $this->prenomE = $prenomE;
    }


    /**
     * @return string
     */
    public function getPoste()
    {
        return $this->poste;
    }

    /**
     * @param string $poste
     */
    public function setPoste($poste)
    {
        $this->poste = $poste;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }


    public function getFile()
    {
        return $this->file;
    }


    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @param string $adresse
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    }

    /**
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * @param string $pays
     */
    public function setPays($pays)
    {
        $this->pays = $pays;
    }

    /**
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * @param string $ville
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
    }

    /**
     * @return string
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * @param string $sexe
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;
    }

    /**
     * @return string
     */
    public function getNiveau()
    {
        return $this->niveau;
    }

    /**
     * @param string $niveau
     */
    public function setNiveau($niveau)
    {
        $this->niveau = $niveau;
    }

    /**
     * @return string
     */
    public function getCin()
    {
        return $this->cin;
    }

    /**
     * @param string $cin
     */
    public function setCin($cin)
    {
        $this->cin = $cin;
    }

    /**
     * @return string
     */
    public function getTeld()
    {
        return $this->teld;
    }

    /**
     * @param string $teld
     */
    public function setTeld($teld)
    {
        $this->teld = $teld;
    }

    /**
     * @return string
     */
    public function getTelp()
    {
        return $this->telp;
    }

    /**
     * @param string telp
     */
    public function setTelp($telp)
    {
        $this->telp = $telp;
    }

    /**
     * @return string
     */
    public function getEmployeur()
    {
        return $this->employeur;
    }

    /**
     * @param string employeur
     */
    public function setEmployeur($employeur)
    {
        $this->employeur = $employeur;
    }


    /**
     * @return string
     */
    public function getLieuC()
    {
        return $this->lieuC;
    }

    /**
     * @param string $lieuC
     */
    public function setLieuC($lieuC)
    {
        $this->lieuC = $lieuC;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getCodepostale()
    {
        return $this->codepostale;
    }

    /**
     * @param string $codepostale
     */
    public function setCodepostale($codepostale)
    {
        $this->codepostale = $codepostale;
    }


    /**
     * @return string
     */
    public function getLieunaissance()
    {
        return $this->lieunaissance;
    }

    /**
     * @param string $lieunaissance
     */
    public function setLieunaissance($lieunaissance)
    {
        $this->lieunaissance = $lieunaissance;
    }

    /**
     * @return date
     */
    public function getDatenaissance()
    {
        return $this->datenaissance;
    }

    /**
     * @param string $datenaissance
     */
    public function setDatenaissance($datenaissance)
    {
        $this->datenaissance = $datenaissance;
    }

    /**
     * @return date
     */
    public function getDateC()
    {
        return $this->dateC;
    }

    /**
     * @param string $dateC
     */
    public function setDateC($dateC)
    {
        $this->dateC = $dateC;
    }



    /**
     * @return mixed
     */
    public function getUserClasse()
    {
        return $this->userClasse;
    }

    /**
     * @param mixed $userClasse
     */
    public function setUserClasse($userClasse)
    {
        $this->userClasse = $userClasse;
    }







    public function addUserClasse(\AppBundle\Entity\SousFiliere $classe)
    {
        $this->userClasse->add($classe);
    }

    public function removeUserClasse(\AppBundle\Entity\SousFiliere $classe)
    {
        $this->userClasse->removeElement($classe);
    }




}