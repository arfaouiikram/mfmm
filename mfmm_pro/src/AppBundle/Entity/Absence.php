<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Absence
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\AbsenceRepository")
 */

class Absence
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Annee", inversedBy="Absence"))
     * @ORM\JoinColumn(name="annee", referencedColumnName="id" , nullable=true)
     */
    private $annee;


    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Filiere", inversedBy="Absence"))
     * @ORM\JoinColumn(name="filiere", referencedColumnName="id" , nullable=true)
     */
    private $filiere;


    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="Absence"))
     * @ORM\JoinColumn(name="user", referencedColumnName="id" , nullable=true)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SousFiliere", inversedBy="Absence"))
     * @ORM\JoinColumn(name="sfiliere", referencedColumnName="id" , nullable=true)
     */
    private $sfiliere;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Formation", inversedBy="Absence"))
     * @ORM\JoinColumn(name="formation", referencedColumnName="id" , nullable=true)
     */
    private $formation;


    /**
     * @var integer
     * @ORM\Column(name="nbheure", type="integer" , length=255 , nullable=true)
     */
    private $nbheure;


    /**
     * @var boolean
     *
     * @ORM\Column(name="justifier", type="boolean", nullable=true)
     */
    private $justifier;


    /**
     * @var boolean
     *
     * @ORM\Column(name="isDeleted", type="boolean", nullable=true)
     */
    private $isDeleted;

    /**
     * @var string
     * @ORM\Column(name="date", type="string" , length=255 , nullable=true)
     */
    private $date;

    /**
     * @var string
     * @ORM\Column(name="debut", type="string", nullable=true)
     */
    private $debut;

    /**
     * @var string
     * @ORM\Column(name="fin", type="string", nullable=true)
     */
    private $fin;

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param boolean $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
    }

    /**
     * @return boolean
     */
    public function getJustifier()
    {
        return $this->justifier;
    }

    /**
     * @param boolean $justifier
     */
    public function setJustifier($justifier)
    {
        $this->justifier = $justifier;
    }


    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return integer
     */
    public function getNbheure()
    {
        return $this->nbheure;
    }

    /**
     * @param integer $nbheure
     */
    public function setNbheure($nbheure)
    {
        $this->nbheure = $nbheure;
    }

    /**
     * @return mixed
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * @param mixed $annee
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;
    }

    /**
     * @return mixed
     */
    public function getSfiliere()
    {
        return $this->sfiliere;
    }

    /**
     * @param mixed $sfiliere
     */
    public function setSfiliere($sfiliere)
    {
        $this->sfiliere = $sfiliere;
    }



    /**
     * @return mixed
     */
    public function getFiliere()
    {
        return $this->filiere;
    }

    /**
     * @param mixed $filiere
     */
    public function setFiliere($filiere)
    {
        $this->filiere = $filiere;
    }


    /**
     * @param mixed $formation
     */
    public function setFormation($formation)
    {
        $this->formation = $formation;
    }

    /**
     * @return mixed
     */
    public function getFormation()
    {
        return $this->formation;
    }

    /**
     * @param mixed $formation
     */
    public function setMatiere($formation)
    {
        $this->formation = $formation;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getDebut()
    {
        return $this->debut;
    }

    /**
     * @param string $debut
     */
    public function setDebut($debut)
    {
        $this->debut = $debut;
    }

    /**
     * @param string $fin
     */
    public function setFin($fin)
    {
        $this->fin = $fin;
    }

    /**
     * @return string
     */
    public function getFin()
    {
        return $this->fin;
    }


}
