<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SousFiliere
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\SousFiliereRepository")
 */

class SousFiliere
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="nom", type="string" , length=255 , nullable=true)
     */
    private $nom;

    /**
     * @var string
     * @ORM\Column(name="nomA", type="string" , length=255 , nullable=true)
     */
    private $nomA;

    /**
     * @var string
     * @ORM\Column(name="nomE", type="string" , length=255 , nullable=true)
     */
    private $nomE;

    /**
     * @var string
     * @ORM\Column(name="code", type="string" , length=255 , nullable=true)
     */
    private $code;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Filiere", inversedBy="User"))
     * @ORM\JoinColumn(name="filiere", referencedColumnName="id" , nullable=true)
     */
    private $filiere;

    /**
     * @var integer
     * @ORM\Column(name="nbJour", type="integer" , length=255 , nullable=true)
     */
    private $nbJour;

    /**
     * @var date
     * @ORM\Column(name="dateD", type="date" , length=255 , nullable=true)
     */
    private $dateD;

    /**
     * @var date
     * @ORM\Column(name="dateF", type="date" , length=255 , nullable=true)
     */
    private $dateF;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Filiere")
     * @ORM\JoinTable(name="sousFiliere_Filiere",
     *      joinColumns={@ORM\JoinColumn(name="sousFiliere_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="filiere_id", referencedColumnName="id", unique=false)}
     *      )
     */
    private $SousFilereFiliere;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinTable(name="Theme_stagiares",
     *      joinColumns={@ORM\JoinColumn(name="theme", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="stagiarie", referencedColumnName="id", unique=false)}
     *      )
     */
    private $Stagiaire;



    /**
     * @return mixed
     */
    public function getStagiaire()
    {
        return $this->Stagiaire;
    }

    /**
     * @param mixed $Stagiaire
     */
    public function setStagiaire($Stagiaire)
    {
        $this->Stagiaire = $Stagiaire;
    }

    public function addStagiaire(\AppBundle\Entity\User $Stagiaire)
    {
        $this->Stagiaire->add($Stagiaire);
    }

    public function removeStagiaire(\AppBundle\Entity\User $Stagiaire)
    {
        $this->Stagiaire->removeElement($Stagiaire);
    }

    /**
     * @return date
     */
    public function getDateD()
    {
        return $this->dateD;
    }

    /**
     * @param string $dateD
     */
    public function setDateD($dateD)
    {
        $this->dateD = $dateD;
    }

    /**
     * @return date
     */
    public function getDateF()
    {
        return $this->dateF;
    }

    /**
     * @param string $dateF
     */
    public function setDateF($dateF)
    {
        $this->dateF = $dateF;
    }


    public function __construct()
    {
        $this->userClasse = new \Doctrine\Common\Collections\ArrayCollection();
        $this->SousFilereFiliere = new \Doctrine\Common\Collections\ArrayCollection();
        $this->Stagiaire = new \Doctrine\Common\Collections\ArrayCollection();

        // your own logic
    }
    /**
     * @return mixed
     */
    public function getSousFilereFiliere()
    {
        return $this->SousFilereFiliere;
    }

    /**
     * @param mixed $SousFilereFiliere
     */
    public function setSousFilereFiliere($SousFilereFiliere)
    {
        $this->SousFilereFiliere = $SousFilereFiliere;
    }

    public function addSousFilereFiliere(\AppBundle\Entity\SousFiliere $classe)
    {
        $this->SousFilereFiliere->add($classe);
    }

    public function removeSousFilereFiliere(\AppBundle\Entity\SousFiliere $classe)
    {
        $this->SousFilereFiliere->removeElement($classe);
    }






    /**
     * @var boolean
     *
     * @ORM\Column(name="isDeleted", type="boolean", nullable=true)
     */
    private $isDeleted;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFiliere()
    {
        return $this->filiere;
    }

    /**
     * @param mixed $filiere
     */
    public function setFiliere($filiere)
    {
        $this->filiere = $filiere;
    }



    /**
     * @return string
     */
    public function getNomE()
    {
        return $this->nomE;
    }

    /**
     * @param string $nomE
     */
    public function setNomE($nomE)
    {
        $this->nomE = $nomE;
    }


    /**
     * @return string
     */
    public function getNomA()
    {
        return $this->nomA;
    }

    /**
     * @param string $nomA
     */
    public function setNomA($nomA)
    {
        $this->nomA = $nomA;
    }

    /**
     * @return boolean
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * @param boolean $isDeleted
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;
    }


    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }


    /**
     * @return integer
     */
    public function getNbJour()
    {
        return $this->nbJour;
    }

    /**
     * @param integer $nbJour
     */
    public function setNbJour($nbJour)
    {
        $this->nbJour = $nbJour;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

}
