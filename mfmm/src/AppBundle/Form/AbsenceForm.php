<?php

namespace AppBundle\Form;



use AppBundle\Entity\AnneeRepository;
use AppBundle\Entity\ClasseRepository;
use AppBundle\Entity\FiliereRepository;
use AppBundle\Entity\MatiereRepository;
use AppBundle\Entity\NiveauRepository;
use AppBundle\Entity\SeanceRepository;
use AppBundle\Entity\SemestreRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class AbsenceForm extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('annee', EntityType::class, array(
                'class' => 'AppBundle:Annee',
                'query_builder' => function (AnneeRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.nom', 'ASC');
                },
                'choice_label' => 'nom',))
            ->add('classe', EntityType::class, array(
                'class' => 'AppBundle:Classe',
                'query_builder' => function (ClasseRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.nom', 'ASC');
                },
                'choice_label' => 'nom',
                'required' =>true))
            ->add('semestre', EntityType::class, array(
                'class' => 'AppBundle:Semestre',
                'query_builder' => function (SemestreRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.nom', 'ASC');
                },
                'choice_label' => 'nom',
                'required' =>true))
            ->add('matiere', EntityType::class, array(
                'class' => 'AppBundle:Matiere',
                'query_builder' => function (MatiereRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.nom', 'ASC');
                },
                'choice_label' => 'nom',
                'required' =>true))
            ->add('filiere', EntityType::class, array(
                'class' => 'AppBundle:Filiere',
                'query_builder' => function (FiliereRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.nom', 'ASC');
                },
                'choice_label' => 'nom',))
            ->add('date', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ))
            ->add('seance', EntityType::class, array(
                'class' => 'AppBundle:Seance',
                'query_builder' => function (SeanceRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.nom', 'ASC');
                },
                'choice_label' => 'nom',))


        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Absence'
        ));
    }

    public function getName()
    {
        return 'absence_form';
    }

}
