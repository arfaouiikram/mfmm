<?php

namespace AppBundle\Form;



use AppBundle\Entity\AnneeRepository;
use AppBundle\Entity\ClasseRepository;
use AppBundle\Entity\FiliereRepository;
use AppBundle\Entity\MatiereRepository;
use AppBundle\Entity\NiveauRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class DevoirForm extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('type', ChoiceType::class, array(
                'choices'  => array(
                    'Cc' => 'Cc',
                    'Ex' => 'Ex',
                ),
                ))
            ->add('coif')
            ->add('annee', EntityType::class, array(
                'class' => 'AppBundle:Annee',
                'query_builder' => function (AnneeRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.nom', 'ASC');
                },
                'choice_label' => 'nom',))
            ->add('classe', EntityType::class, array(
                'class' => 'AppBundle:Classe',
                'query_builder' => function (ClasseRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.nom', 'ASC');
                },
                'choice_label' => 'nom',))
            ->add('date', DateType::class, array(
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ))
            ->add('matiere', EntityType::class, array(
                'class' => 'AppBundle:Matiere',
                'query_builder' => function (MatiereRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.isDeleted=1')
                        ->orderBy('u.nom', 'ASC');
                },
                'choice_label' => 'nom',))

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Devoir'
        ));
    }

    public function getName()
    {
        return 'devoir_form';
    }

}
