<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Annee;
use AppBundle\Entity\Classe;
use AppBundle\Entity\Devoir;
use AppBundle\Entity\Emplois;
use AppBundle\Entity\Filiere;
use AppBundle\Entity\Groupe;
use AppBundle\Entity\Matiere;
use AppBundle\Entity\Niveau;
use AppBundle\Entity\Semestre;
use AppBundle\Form\AnneeForm;
use AppBundle\Form\ClasseForm;
use AppBundle\Form\DevoirForm;
use AppBundle\Form\EmploisForm;
use AppBundle\Form\FiliereForm;
use AppBundle\Form\GroupeForm;
use AppBundle\Form\MatiereForm;
use AppBundle\Form\NiveauForm;
use AppBundle\Form\SemestreForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EtudiantController extends Controller
{

    public function indexAction(Request $request,$annee)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $niveaux = $em->getRepository('AppBundle:Niveau')->findAll();
        $results=array();
        $users = $em->createQueryBuilder()
            ->select('u')
            ->from('AppBundle:User','u')
            ->where('u.isDeleted=1')
            ->getQuery()
            ->getResult();
        $role=$this->container->getParameter('security.role_hierarchy.roles');
        foreach($niveaux as $niveau){
            $pagination=array();
            foreach($users as $user){
                if($user->hasRole($role['ROLE_ETUDIANT'][0]) && !($user->isSuperAdmin())){
                    foreach($user->getUserClasse() as $classe){
                        if($classe->getNiveau()==$niveau && $classe->getAnnee()->getId()==$annee){
                            $pagination[]=['user'=>$user,'classe'=>$classe];
                        }
                    }
                }
            }
            $results[]=['niveau'=>$niveau,'etudiants'=>$pagination];
        }
        return $this->render("@AppBundle/Resources/views/etudiant/list.html.twig",array('pagination' => $results));
    }

    public function anneeEtudiantAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $emplois = $em->createQueryBuilder()
            ->select('e')
            ->from('AppBundle:Annee', 'e')
            ->where('e.isDeleted=1')
            ->getQuery()
            ->getResult();
        return $this->render("@AppBundle/Resources/views/etudiant/anneeToSelect.html.twig",array('annees' => $emplois));
    }

    public function ajoutEtudiantAction(Request $request){
        $roles=$this->container->getParameter('security.role_hierarchy.roles');
        $role=$roles['ROLE_ETUDIANT'];
        $displayAgent='none';
        $displayEtudiant='block';
        $displayEnseignant='none';
        return $this->redirectToRoute("fos_user_registration_register",array('role'=>$role,'displayAgent'=>$displayAgent,'displayEnseignant'=>$displayEnseignant,'displayEtudiant'=>$displayEtudiant));
    }

    public function modifierEtudiantAction(Request $request ,$id){
        $displayAgent='none';
        $displayEtudiant='block';
        $displayEnseignant='none';
        return $this->redirectToRoute("fos_user_profile_edit",array('user'=>$id,'displayAgent'=>$displayAgent,'displayEnseignant'=>$displayEnseignant,'displayEtudiant'=>$displayEtudiant));
    }
    public function supprimerEtudiantAction(Request $request ,$id){
        $em = $this->get('doctrine.orm.entity_manager');
        $user = $em->getRepository('AppBundle:User')->find($id);
        $user->setIsDeleted(0);
        $em->persist($user);
        $em->flush();
        return $this->redirectToRoute("mfmm_crm_annee_to_etudiants");

    }

    public function getClasseAction(Request $request ,$annee,$filiere){
        $em = $this->getDoctrine()->getManager();
        $an = $em->getRepository('AppBundle:Annee')->find($annee);
        $fil = $em->getRepository('AppBundle:Filiere')->find($filiere);
        $classes = $em->getRepository('AppBundle:Classe')->findBy(array(
            'annee' => $an,
            'filiere'=>$fil
        ));
        $res=array();
        foreach($classes as $classe){
            $res[]=['id'=>$classe->getId(),'nom'=>$classe->getNom()];
        }
        return new JsonResponse($res);
    }

    public function carteEtudiantAction(Request $request,$id,$annee,$classe)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $user = $em->getRepository('AppBundle:User')->find($id);
        $annee = $em->getRepository('AppBundle:Annee')->find($annee);
        $classe = $em->getRepository('AppBundle:Classe')->find($classe);
        $template = 'carte.html.twig';
        $filename = sprintf('convention-%s.pdf', $user->getId());
        $html = $this->renderView($template,array('user'=>$user,'annee'=>$annee,'classe'=>$classe));
        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html)
            ,
            200,
            [
                'Content-Type'        => 'application/pdf',
                'Content-Disposition' => sprintf('inline; filename="%s"', $filename),
            ]
        );
    }

    public function bulletinAction(Request $request,$id,$annee,$classe)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $classe = $em->getRepository('AppBundle:Classe')->find($classe);
        $annee = $em->getRepository('AppBundle:Annee')->find($annee);
        $user = $em->getRepository('AppBundle:User')->find($id);
        $semestres = $em->getRepository('AppBundle:Semestre')->findAll();
        $notes = $em->getRepository('AppBundle:Note')->findBy(array('classe'=>$classe,'user'=>$user));
        $devoirs = $em->getRepository('AppBundle:Devoir')->findBy(array('classe'=>$classe,'isDeleted'=>1));
        $res=array();
        foreach($semestres as $semestre){
            $pagination=array();
            $moyGen=0;
            $matieres = $em->getRepository('AppBundle:Matiere')->findBy(array('classe'=>$classe,'isDeleted'=>1,'semestre'=>$semestre));
            $resMatGlobel=array();
            $coif=0;
            $coifGen=0;
            foreach($matieres as $mat){
                $resMat=array();
                $moyenne=0;
                $i=0;
                $j=0;
                foreach($notes as $no){
                    if($no->getMatiere()==$mat ){
                        if($no->getDevoir()->getType()=='Ex'){
                            $resMat['Ex']=$no->getNote();
                            $moyenne+=$no->getNote()*2;
                            $i++;
                        }else{
                            $resMat['Cc']=$no->getNote();
                            $moyenne+=$no->getNote();
                            $j++;
                        }
                        $coif=$no->getDevoir()->getCoif();
                        $coifGen+=$no->getDevoir()->getCoif();
                    }
                }
                if($i==0){
                    $resMat['Ex']=0;
                }
                if($j==0){
                    $resMat['Cc']=0;
                }
                $resMat['Moy']=$moyenne/3;
                $moyGen+=($moyenne/3)*$coif;
                $resMatGlobel[]=['Matiere'=>$mat->getNom(),'notes'=>$resMat];
            }
            if($coifGen==0){
                $coifGen=1;
            }
            $res[]=['semestre'=>$semestre->getNom(),'dev'=>$resMatGlobel,'moyenne'=>$moyGen/($coifGen)];

        }
        return $this->render("@AppBundle/Resources/views/etudiant/bulletin.html.twig",array('pagination'=>$res));

    }

    public  function classeAction(Request $request){
        $user=$this->getUser();
        if($user){
            return $this->render("@AppBundle/Resources/views/etudiant/classes.html.twig",array('pagination'=>$user->getUserClasse()));
        }
        return $this->redirectToRoute('fos_user_security_login');
    }

    public function notesAction(Request $request,$classe){
        $user=$this->getUser();
        $em = $this->get('doctrine.orm.entity_manager');
        $classe = $em->getRepository('AppBundle:Classe')->find($classe);
        $semestre = $em->getRepository('AppBundle:Semestre')->findAll();
        $tab=array();
        $notes = $em->getRepository('AppBundle:Note')->findBy(array('classe'=>$classe,'user'=>$user));
        foreach($semestre as $se){
            $page=array();
            $page1=array();
            foreach($notes as $no){
                if($no->getMatiere()->getSemestre()==$se){
                    $page[]=$no;
                }else{
                    $page1[]=$no;
                }
            }
        }
        $tab[]=['semestre'=>'Semestre1','pages'=>$page];
        $tab[]=['semestre'=>'Semestre2','pages'=>$page1];
        return $this->render("@AppBundle/Resources/views/etudiant/notes.html.twig",array('pagination'=>$tab));
    }




}
