<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        if($this->getUser()){
            $em = $this->get('doctrine.orm.entity_manager');
            $paiements = $em->getRepository('AppBundle:Paiement')->findBy(array('isDeleted'=>1));
            $query = $this->getDoctrine()->getManager()
                ->createQuery(
                    'SELECT u FROM AppBundle:User u WHERE u.isDeleted=1 AND u.roles LIKE :role'
                )->setParameter('role', '%"ROLE_ETUDIANT"%');
            $etudiants = $query->getResult();
            $query1 = $this->getDoctrine()->getManager()
                ->createQuery(
                    'SELECT u FROM AppBundle:User u WHERE u.isDeleted=1 AND u.roles LIKE :role'
                )->setParameter('role', '%"ROLE_ENSEIGNANT"%');
            $enseignants = $query1->getResult();
            $query2 = $this->getDoctrine()->getManager()
                ->createQuery(
                    'SELECT u FROM AppBundle:User u WHERE u.isDeleted=1 AND u.roles LIKE :role'
                )->setParameter('role', '%"ROLE_AGENT"%');
            $agents = $query2->getResult();
            return $this->render('default/index.html.twig',array('etudiants'=>sizeof($etudiants),'enseigants'=>sizeof($enseignants),'agents'=>sizeof($agents),'paiements'=>sizeof($paiements)));
        }else{
            return $this->redirectToRoute('fos_user_security_login');
        }

    }
}
