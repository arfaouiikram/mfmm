<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Absence;
use AppBundle\Entity\Attestation;
use AppBundle\Entity\Emplois;
use AppBundle\Form\AbsenceEtatForm;
use AppBundle\Form\AbsenceForm;
use AppBundle\Form\EmploisForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AttestationController extends Controller
{

   public function demandeAction(Request $request){
       $user=$this->getUser();
       if($user){
           $em = $this->get('doctrine.orm.entity_manager');
           $classes=$user->getUserClasse();
           $pagination=array();
           foreach($classes as $classe){
               $pagination[$classe->getNom()]=$classe->getId();
           }
           $form = $this->createFormBuilder()
               ->add('classe', ChoiceType::class,array('choices'=>$pagination))
               ->getForm();
           if($request->getMethod()=='POST'){
               $form=$request->request->get('form');
               $class=$form['classe'];
               if($class!=0 && $class!=null){
                   $classe = $em->getRepository('AppBundle:Classe')->find($class);
                   $attestation=new Attestation();
                   $attestation->setUser($user);
                   $attestation->setClasse($classe);
                   $attestation->setIsDeleted(1);
                   $attestation->setEtat(0);
                   $em->persist($attestation);
                   $em->flush();
                   return $this->redirectToRoute('mfmm_crm_list_attestation');
               }
           }
           return $this->render("@AppBundle/Resources/views/attestation/demande.html.twig",array('classes' => $classes,'form'=>$form->createView()));
       }else{
           return $this->redirectToRoute('fos_user_security_login');
       }
   }

    public function listeAction(Request $request){
        $user=$this->getUser();
        $em = $this->get('doctrine.orm.entity_manager');
        if($user){
            if($user->hasRole('ROLE_ETUDIANT')){
                $role=1;
                $attestations = $em->getRepository('AppBundle:Attestation')->findBy(array('isDeleted'=>1,'user'=>$user));
            }else{
                $role=0;
                $attestations = $em->getRepository('AppBundle:Attestation')->findBy(array('isDeleted'=>1));
            }
            return $this->render("@AppBundle/Resources/views/attestation/list.html.twig",array('pagination' => $attestations,'role'=>$role));
        }else{
            return $this->redirectToRoute('fos_user_security_login');
        }
    }

    public function etatAction(Request $request,$id){
        $em = $this->get('doctrine.orm.entity_manager');
        $attestation = $em->getRepository('AppBundle:Attestation')->find($id);
        if($attestation->getEtat()==0){
            $attestation->setEtat(1);
        }else{
            $attestation->setEtat(0);
        }
        $em->persist($attestation);
        $em->flush();
        return $this->redirectToRoute('mfmm_crm_list_attestation');
    }

    public function imprimerAttestationAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $attestation = $em->getRepository('AppBundle:Attestation')->find($id);
        $template = 'attestationF.html.twig';
        $filename = sprintf('Attestation-%s.pdf', $attestation->getUser()->getId());
        $html = $this->renderView($template,array('user'=>$attestation->getUser(),'classe'=>$attestation->getClasse()));
        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html)
            ,
            200,
            [
                'Content-Type'        => 'application/pdf',
                'Content-Disposition' => sprintf('inline; filename="%s"', $filename),
            ]
        );
    }





}
