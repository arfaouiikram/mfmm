<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Absence;
use AppBundle\Entity\Emplois;
use AppBundle\Form\AbsenceEtatForm;
use AppBundle\Form\AbsenceForm;
use AppBundle\Form\EmploisForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AbsenceController extends Controller
{

    public function absenceEtudiantsAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $absence=new Absence();
        $display='none';
        $form = $this->createForm(AbsenceForm::class, $absence);
        $form->handleRequest($request);
        $etudiants=array();
        $pagination=array();
        if($request->getMethod()=='POST'){
            if($form->isValid()){
                $absence=$form->getData();
                $users = $em->getRepository('AppBundle:User')->findAll();
                $role=$this->container->getParameter('security.role_hierarchy.roles');
                foreach($users as $user) {
                    if ($user->hasRole($role['ROLE_ETUDIANT'][0]) && !($user->isSuperAdmin())) {
                        foreach($user->getUserClasse() as $classe){
                            if($classe==$absence->getClasse()){
                                $etudiants[]=$user;
                            }
                        }
                    }
                }
                $display='block';
                $date=$absence->getDate()->format('Y-m-d');
                $absences = $em->getRepository('AppBundle:Absence')->findBy(array('classe'=>$absence->getClasse(),'matiere'=>$absence->getMatiere(),'semestre'=>$absence->getSemestre(),'date'=>$date));
                $pagination=array();
                foreach($etudiants as $etud){
                    $i=0;
                    $absencesTotal = $em->getRepository('AppBundle:Absence')->findBy(array('classe'=>$absence->getClasse(),'matiere'=>$absence->getMatiere(),'semestre'=>$absence->getSemestre(),'user'=>$etud,'nbheure'=>1));
                    $count=sizeof($absencesTotal);
                    foreach($absences as $ab){
                        if($ab->getUser()==$etud){
                            $pagination[]=['etudiant'=>$etud,'absence'=>$ab->getNbHeure(),'justifie'=>$ab->getJustifier(),'count'=>$count];
                            $i++;
                        }
                    }
                    if($i==0){
                        $pagination[]=['etudiant'=>$etud,'absence'=>0,'justifie'=>false,'count'=>$count];
                    }
                }
                return $this->render("@AppBundle/Resources/views/absence/list1.html.twig",array('form' =>$form->createView(),'pagination'=>$pagination,'display'=>$display));

            }
        }
        return $this->render("@AppBundle/Resources/views/absence/list.html.twig",array('form' =>$form->createView(),'pagination'=>$pagination,'display'=>$display));
    }

    public function getMatiereAction(Request $request ,$classe,$semestre){
        $em = $this->getDoctrine()->getManager();
        $classe = $em->getRepository('AppBundle:Classe')->find($classe);
        $semestre = $em->getRepository('AppBundle:Filiere')->find($semestre);
        $matieres = $em->getRepository('AppBundle:Matiere')->findBy(array(
            'classe' => $classe,
            'semestre'=>$semestre
        ));
        $res=array();
        if(sizeof($matieres)!=0){
            foreach($matieres as $mat){
                $res[]=['id'=>$mat->getId(),'nom'=>$mat->getNom()];
            }
        }else{
            $res[]=['id'=>0,'nom'=>'aucune matière'];
        }

        return new JsonResponse($res);
    }

    public function ajoutAbsenceClasseAction(Request $request,$ligne,$classe,$matiere,$semestre,$date,$seance){
        $col=split(',',$ligne);
        $tab=array();
        $em = $this->getDoctrine()->getManager();
        $classe = $em->getRepository('AppBundle:Classe')->find($classe);
        $semestre = $em->getRepository('AppBundle:Semestre')->find($semestre);
        $matiere = $em->getRepository('AppBundle:Matiere')->find($matiere);
        $seance = $em->getRepository('AppBundle:Seance')->find($seance);
        foreach($col as $c){
            if($c!=""){
                $ab=split(':',$c);
                $tab[]=['etudiant'=>$ab[0],'absence'=>$ab[1],'justifie'=>$ab[2]];
            }
        }
        $absences = $em->getRepository('AppBundle:Absence')->findBy(array('classe'=>$classe,'matiere'=>$matiere,'semestre'=>$semestre,'date'=>$date));
        foreach($absences as $ab){
            $em->remove($ab);
            $em->flush();
        }
        foreach($tab as $t){
            $absence=new Absence();
            $user = $em->getRepository('AppBundle:User')->find($t['etudiant']);
            $absence->setClasse($classe);
            $absence->setMatiere($matiere);
            $absence->setSemestre($semestre);
            $absence->setUser($user);
            $absence->setDate($date);
            $absence->setSeance($seance);
            $absence->setNbheure($t['absence']);
            if($t['justifie']=="true"){
                $jus=true;
            }else{
                $jus=false;
            }
            $absence->setJustifier($jus);
            $absence->setIsDeleted(1);
            $em->persist($absence);
        }
        $em->flush();
        return $this->redirectToRoute("mfmm_crm_anbsences_etudiants");
    }

    public function etatAbsencesClasseAction(Request $request){
        $em = $this->get('doctrine.orm.entity_manager');
        $absence=new Absence();
        $display='none';
        $form = $this->createForm(AbsenceEtatForm::class, $absence);
        $form->handleRequest($request);
        $etudiants=array();
        $pages=array();
        $somme=0;
        $paiements=array();
        $matieres=array();
        $pagination=array();
        if($request->getMethod()=='POST') {
            if ($form->isValid()) {
                $users = $em->getRepository('AppBundle:User')->findAll();
                $role=$this->container->getParameter('security.role_hierarchy.roles');
                foreach($users as $user) {
                    if ($user->hasRole($role['ROLE_ETUDIANT'][0]) && !($user->isSuperAdmin())) {
                        foreach($user->getUserClasse() as $classe){
                            if($classe==$absence->getClasse()){
                                $etudiants[]=$user;
                            }
                        }
                    }
                }$pages=array();
                $matieres = $em->getRepository('AppBundle:Matiere')->findBy(array('classe'=>$absence->getClasse(),'semestre'=>$absence->getSemestre()));
                foreach($etudiants as $etud){
                    $pagination=array();
                    foreach($matieres as $mat){
                        $absencesEtud = $em->getRepository('AppBundle:Absence')->findBy(array('classe'=>$absence->getClasse(),'semestre'=>$absence->getSemestre(),'user'=>$etud,'matiere'=>$mat,'nbheure'=>1));
                        $paiements = $em->getRepository('AppBundle:Paiement')->findBy(array('user'=>$etud,'classe'=>$classe,'isDeleted'=>1));
                        $somme=0;
                        foreach($paiements  as $pay){
                            $somme+=$pay->getPrix();
                        }
                        $pagination[]=['matiere'=>$mat->getNom(),'absences'=>sizeof($absencesEtud)];
                    }
                    $pages[]=['etudiant'=>$etud,'page'=>$pagination,'somme'=>$somme,'prix'=>$classe->getPrix(),'tranche'=>sizeof($paiements)];
                }
                $display='block';
            }
        }
        return $this->render("@AppBundle/Resources/views/absence/listEtat.html.twig",array('form' =>$form->createView(),'pagination'=>$pages,'display'=>$display,'matieres'=>$matieres));
    }

    public function SelectAnneeAction(Request $request){

        $em = $this->get('doctrine.orm.entity_manager');
        $emplois = $em->createQueryBuilder()
            ->select('e')
            ->from('AppBundle:Annee', 'e')
            ->where('e.isDeleted=1')
            ->getQuery()
            ->getResult();
        return $this->render("@AppBundle/Resources/views/absence/anneeToSelect.html.twig",array('annees' => $emplois));
    }


    public function selectClassAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $classes = $em->createQueryBuilder()
            ->select('c')
            ->from('AppBundle:Classe', 'c')
            ->join('c.annee','a')
            ->where('a.id='.$id)
            ->andwhere('c.isDeleted=1')
            ->getQuery()
            ->getResult();
        return $this->render("@AppBundle/Resources/views/absence/classeToSelect.html.twig",array('classes' => $classes));
    }


    public function etatModuleAction(Request $request ,$id){

        $em = $this->get('doctrine.orm.entity_manager');
        $calsse = $em->getRepository('AppBundle:Classe')->find($id);
        $matieres = $em->getRepository('AppBundle:Matiere')->findBy(array('classe'=>$calsse));
        $total=0;
        $reste=0;
        foreach($matieres as $ma){
            $total+=$ma->getTotalHeure();
            $reste+=$ma->getNbheure();
        }
        return $this->render("@AppBundle/Resources/views/absence/etatModule.html.twig",array('pagination' => $matieres,'total'=>$total,'reste'=>$reste));
    }




}
