<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Annee;
use AppBundle\Entity\Classe;
use AppBundle\Entity\Devoir;
use AppBundle\Entity\Emplois;
use AppBundle\Entity\Filiere;
use AppBundle\Entity\Groupe;
use AppBundle\Entity\Matiere;
use AppBundle\Entity\Niveau;
use AppBundle\Entity\Semestre;
use AppBundle\Form\AnneeForm;
use AppBundle\Form\ClasseForm;
use AppBundle\Form\DevoirForm;
use AppBundle\Form\EmploisForm;
use AppBundle\Form\FiliereForm;
use AppBundle\Form\GroupeForm;
use AppBundle\Form\MatiereForm;
use AppBundle\Form\NiveauForm;
use AppBundle\Form\SemestreForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;

class EmploisController extends Controller
{

    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $emplois = $em->createQueryBuilder()
            ->select('e')
            ->from('AppBundle:Annee', 'e')
            ->where('e.isDeleted=1')
            ->getQuery()
            ->getResult();
         return $this->render("@AppBundle/Resources/views/emplois/anneeToSelect.html.twig",array('annees' => $emplois));
    }

    public function listClasseAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $classes = $em->createQueryBuilder()
            ->select('c')
            ->from('AppBundle:Classe', 'c')
            ->join('c.annee','a')
            ->where('a.id='.$id)
            ->andwhere('c.isDeleted=1')
            ->getQuery()
            ->getResult();
        return $this->render("@AppBundle/Resources/views/emplois/classeToSelect.html.twig",array('classes' => $classes));
    }

    public function listEmploisSemaineAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $emplois = $em->createQueryBuilder()
            ->select('c')
            ->from('AppBundle:Emplois', 'c')
            ->join('c.classe','a')
            ->where('a.id='.$id)
            ->andwhere('c.isDeleted=1')
            ->groupBy('c.dateD')
            ->orderBy('c.dateD', 'ASC')
            ->getQuery()
            ->getResult();
        $semstres=$em->getRepository("AppBundle:Semestre")->findBy(array('isDeleted'=>1));
        $res=array();
        foreach($semstres as $se){
            $pagination=array()
;            foreach($emplois as $emp){
                if($se==$emp->getSemestre()){
                    $pagination[]=['emplois'=>$emp];
                }
            }
            $res[]=['page'=>$pagination,'semestre'=>$se];
        }
        return $this->render("@AppBundle/Resources/views/emplois/semaineToSelect.html.twig",array('semaines' => $res,'classe'=>$id));
    }

    public function selectindexAction(Request $request ,$annee)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $annees=$em->getRepository("AppBundle:Annee")->find($annee);
        $emplois=new Emplois();
        $display="none";
        $seancess=$em->getRepository("AppBundle:Seance")->findAll();
        $classes = $em->createQueryBuilder()
            ->select('c')
            ->from('AppBundle:Classe', 'c')
            ->join('c.annee','a')
            ->where('a.id='.$annee)
            ->getQuery()
            ->getResult();
        $emplois->setClasse($classes);
        $emplois->setAnnee($annees);
        $form = $this->createForm(EmploisForm::class, $emplois);
        $form->handleRequest($request);
        $classe=0;
        $semestre=0;
        if($request->getMethod()=='POST'){
            $emplois=$form->getData();
            $jours=$em->getRepository("AppBundle:Jour")->findAll();
            $seances=$em->getRepository("AppBundle:Seance")->findAll();
            if($emplois->getAnnee()!= null  && $emplois->getClasse()!=null && $emplois->getSemestre()!=null){
                $pagination = $em->createQueryBuilder()
                    ->select('e')
                    ->from('AppBundle:Emplois', 'e')
                    ->join('e.annee','a')
                    ->join('e.classe','c')
                    ->join('e.semestre','s')
                    ->where('a.id='.$emplois->getAnnee()->getId())
                    ->andwhere('c.id='.$emplois->getClasse()->getId())
                    ->andwhere('s.id='.$emplois->getSemestre()->getId())
                    ->andwhere('e.isDeleted=1')
                    ->getQuery()
                    ->getResult();
                $classe=$emplois->getClasse()->getId();
                $semestre=$emplois->getSemestre()->getId();
                $emplois=array();
                if($pagination){
                    foreach($jours as $jour){
                        $sea=array();
                        foreach($seances as $seance){
                            foreach($pagination as $ligne){
                                if(($ligne->getJour()->getId()==$jour->getId()) && ($ligne->getSeance()->getId()==$seance->getId())){
                                    $sea[]=['matiere'=>$ligne->getMatiere()];
                                    $display="block";
                                }
                            }
                        }
                        $emplois[]=['jour'=>$jour,'seances'=>$sea];
                    }
                }
            }
        }
        return $this->render("@AppBundle/Resources/views/emplois/classeToList.html.twig",array('form' => $form->createView(),'pagination'=>$emplois,'seances'=>$seancess,'display'=>$display,'classe'=>$classe,'semestre'=>$semestre));
    }

    public function ajoutEmploisAction(Request $request,$id)
    {
        if($this->getUser()){
            $em = $this->get('doctrine.orm.entity_manager');
            $classe=$em->getRepository("AppBundle:Classe")->find($id);
            $emplois=new Emplois();
            $emplois->setClasse($classe);
            $emplois->setAnnee($classe->getAnnee());
            $form = $this->createForm(EmploisForm::class, $emplois);
            $jour=$em->getRepository("AppBundle:Jour")->findAll();
            $seance=$em->getRepository("AppBundle:Seance")->findAll();
            $salles=$em->getRepository("AppBundle:Salle")->findAll();
            $users=$em->getRepository("AppBundle:User")->findAll();
            $professeurs=array();
            $professeurs[]=['id'=>0,'nom'=>'Selectionner Professeur'];
            foreach($users as $user){
                if($user->hasRole('ROLE_ENSEIGNANT')){
                    $professeurs[]=$user;
                }
            }
            $sa=array();
            $sa[]=['id'=>0,'nom'=>'Selectionner une salle'];
            foreach($salles as $s ){
                $sa[]=$s;
            }
            $matiere=$em->getRepository("AppBundle:Matiere")->findAll();
            $form->handleRequest($request);
            if($request->getMethod()=="POST"){
                $emplois=$form->getData();
                $select=$request->request->get('sel');
                $prof=$request->request->get('prof');
                $salle=$request->request->get('salle');
                $classe=$emplois->getClasse();
                $annee=$emplois->getAnnee();
                $semestre=$emplois->getSemestre();
                $semaine=$emplois->getSemaine();
                $dateD=$emplois->getDateD();
                $dateF=$emplois->getDateF();
                $emplois=$em->getRepository("AppBundle:Emplois")->findBy(array('classe'=>$classe));
                foreach($emplois as $emp){
                    if($dateD>=$emp->getDateD() && $dateD<=$emp->getDateF()){
                        $flash = array(
                            'key' => 'warning',
                            'title' => 'Echeck',
                            'msg' => "Vous avez déjà remplir cette semaine ");
                        $this->setFlash($flash);
                        return $this->redirectToRoute("mfmm_crm_emplois_emploi_semaine",array('id'=>$classe->getId()));

                    }
                }
                foreach($jour as $j){
                    foreach($seance as $s){
                        $id_matiere=$select[$j->getId()][$s->getId()];
                        $id_Salle=$salle[$j->getId()][$s->getId()];
                        $id_prof=$prof[$j->getId()][$s->getId()];
                        $presence=$request->request->get('pres'.$j->getId().''.$s->getId());
                        $mat=$em->getRepository("AppBundle:Matiere")->find(intval($id_matiere));
                        $sa=$em->getRepository("AppBundle:Salle")->find(intval($id_Salle));
                        $pro=$em->getRepository("AppBundle:User")->find(intval($id_prof));
                        $se=$em->getRepository("AppBundle:Seance")->find(intval($s->getId()));
                        $jou=$em->getRepository("AppBundle:Jour")->find(intval($j->getId()));
                        $emplois=new Emplois();
                        $emplois->setAnnee($annee);
                        if($presence==null){
                            $emplois->setPresent(0);
                        }else{
                            $emplois->setPresent(1);
                            if($mat){
                                $mat->setNbheure($mat->getNbheure()-2);
                                $em->persist($mat);
                            }
                        }
                        $emplois->setClasse($classe);
                        $emplois->setSemestre($semestre);
                        $emplois->setSemaine($semaine);
                        $emplois->setJour($jou);
                        $emplois->setMatiere($mat);
                        $emplois->setSalle($sa);
                        $emplois->setUser($pro);
                        $emplois->setDateD($dateD);
                        $emplois->setDateF($dateF);
                        $emplois->setSeance($se);
                        $emplois->setIsDeleted(1);
                        $emplois->setCreatedBy($this->getUser());
                        $em->persist($emplois);
                    }
                }
                $em->flush();
                return $this->redirectToRoute("mfmm_crm_emplois_emploi_semaine",array('id'=>$classe->getId()));
            }
            return $this->render("@AppBundle/Resources/views/emplois/ajout.html.twig",array('form' => $form->createView(),'jours'=>$jour,'seances'=>$seance,'matieres'=>$matiere,'professeurs'=>$professeurs,'salles'=>$sa));

        }else{
            return $this->redirectToRoute('fos_user_security_login');
        }
    }

    protected function setFlash($value) {
        $this->container->get('session')->getFlashBag()->add('emplois', $value);
    }

    public function acceeEmploisAction(Request $request,$classe,$dateD){
        $em = $this->get('doctrine.orm.entity_manager');
        $classe = $em->getRepository('AppBundle:Classe')->find($classe);
        $dateD=new \DateTime($dateD);
        $pagination = $em->getRepository('AppBundle:Emplois')->findBy(array('classe'=>$classe,'dateD'=>$dateD,'isDeleted'=>1));
        $emploi=new Emplois();
        $emploi->setClasse($classe);
        $emploi->setAnnee($classe->getAnnee());
        $emploi->setSemestre($pagination?$pagination[0]->getSemestre():null);
        $emploi->setSemaine($pagination?$pagination[0]->getSemaine():null);
        $emploi->setDateD($pagination?$pagination[0]->getDateD():null);
        $emploi->setDateF($pagination?$pagination[0]->getDateF():null);
        $form = $this->createForm(EmploisForm::class, $emploi);
        $jours=$em->getRepository("AppBundle:Jour")->findAll();
        $seances=$em->getRepository("AppBundle:Seance")->findAll();
        $salles=$em->getRepository("AppBundle:Salle")->findAll();
        $users=$em->getRepository("AppBundle:User")->findAll();
        $professeurs=array();
        foreach($users as $user){
            if($user->hasRole('ROLE_ENSEIGNANT')){
                $professeurs[]=$user;
            }
        }
        $matieres=$em->getRepository("AppBundle:Matiere")->findBy(array('classe'=>$classe));
        $emplois=array();
        if($pagination){
            foreach($jours as $jour){
                $sea=array();
                foreach($seances as $seance){
                    foreach($pagination as $ligne){
                        if(($ligne->getJour()->getId()==$jour->getId()) && ($ligne->getSeance()->getId()==$seance->getId())){
                            $sea[]=['matiere'=>$ligne->getMatiere(),'seance'=>$ligne->getSeance(),'emplois'=>$ligne,'prof'=>$ligne->getUser(),'salle'=>$ligne->getSalle()];
                        }
                    }
                }
                $emplois[]=['jour'=>$jour,'seances'=>$sea];
            }
        }
        return $this->render("@AppBundle/Resources/views/emplois/classeToList.html.twig",array('form' => $form->createView(),'pagination'=>$emplois,'seances'=>$seances,'classe'=>$classe->getId(),'dateD'=>$dateD,'matieres'=>$matieres,'professeurs'=>$professeurs,'salles'=>$salles));
    }

    public function modifierEmploisAction(Request $request,$classe,$dateD)
    {
        if($this->getUser()){
            $em = $this->get('doctrine.orm.entity_manager');
            $classe = $em->getRepository('AppBundle:Classe')->find($classe);
            $dateD=new \DateTime($dateD);
            $jours = $em->getRepository('AppBundle:Jour')->findAll();
            $seances = $em->getRepository('AppBundle:Seance')->findAll();
            $pagination = $em->getRepository('AppBundle:Emplois')->findBy(array('classe'=>$classe,'dateD'=>$dateD,'isDeleted'=>1));
            $select=$request->request->get('sel');
            $prof=$request->request->get('prof');
            $salle=$request->request->get('salle');
            foreach($jours as $j){
                foreach($seances as $s){
                    $id_matiere=$select[$j->getId()][$s->getId()];
                    $id_Salle=$salle[$j->getId()][$s->getId()];
                    $id_prof=$prof[$j->getId()][$s->getId()];
                    $presence=$request->request->get('pres'.$j->getId().''.$s->getId());
                    $mat=$em->getRepository("AppBundle:Matiere")->find(intval($id_matiere));
                    $pro=$em->getRepository("AppBundle:User")->find(intval($id_prof));
                    $sa=$em->getRepository("AppBundle:Salle")->find(intval($id_Salle));
                    $se=$em->getRepository("AppBundle:Seance")->find(intval($s->getId()));
                    $jou=$em->getRepository("AppBundle:Jour")->find(intval($j->getId()));
                    $emplois=$em->getRepository('AppBundle:Emplois')->findBy(array('classe'=>$classe,'dateD'=>$dateD,'jour'=>$jou,'seance'=>$se,'isDeleted'=>1));
                    if(sizeof($emplois)!=0){
                        if($mat==$emplois[0]->getMatiere()){
                            if($presence==null && $emplois[0]->getPresent()==1){
                                $mat->setNbheure($mat->getNbheure()+2);
                                $em->persist($mat);
                                $emplois[0]->setPresent(0);
                            }
                            if($presence!=null && $emplois[0]->getPresent()==0){
                                $mat->setNbheure($mat->getNbheure()-2);
                                $em->persist($mat);
                                $emplois[0]->setPresent(1);
                            }
                        }else{
                            if($presence==null){
                                $emplois[0]->setPresent(0);
                            }else{
                                $emplois[0]->setPresent(1);
                                $mat->setNbheure($mat->getNbheure()-2);
                                $em->persist($mat);
                            }
                        }
                        $emplois[0]->setJour($jou);
                        $emplois[0]->setMatiere($mat);
                        $emplois[0]->setUser($pro);
                        $emplois[0]->setSalle($sa);
                        $emplois[0]->setSeance($se);
                        $emplois[0]->setIsDeleted(1);
                        $emplois[0]->setCreatedBy($this->getUser());
                        $em->persist($emplois[0]);
                    }
                }
            }
            $classe->setCount($classe->getCount()+1);
            $em->flush();

            return $this->redirectToRoute("mfmm_crm_emplois_emploi_semaine",array('id'=>$classe->getId()));
        }else{
            return $this->redirectToRoute('fos_user_security_login');
        }
    }

    public function supprimerEmploisAction(Request $request,$classe,$dateD)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $classe = $em->getRepository('AppBundle:Classe')->find($classe);
        $dateD=new \DateTime($dateD);
        $emplois = $em->getRepository('AppBundle:Emplois')->findBy(array('classe'=>$classe,'dateD'=>$dateD));
        foreach($emplois as $emp){
            $emp->setIsDeleted(0);
            $em->persist($emp);
        }
        $em->flush();
        return $this->redirectToRoute("mfmm_crm_emplois_emploi_semaine",array('id'=>$classe->getId()));

    }

    public function imprimerEmploisAction(Request $request,$classe,$dateD)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $classe = $em->getRepository('AppBundle:Classe')->find($classe);
        $jours = $em->getRepository('AppBundle:Jour')->findAll();
        $seances = $em->getRepository('AppBundle:Seance')->findAll();
        $matieres=$em->getRepository("AppBundle:Matiere")->findBy(array('classe'=>$classe));
        $dateD=new \DateTime($dateD);
        $pagination = $em->getRepository('AppBundle:Emplois')->findBy(array('classe'=>$classe,'dateD'=>$dateD,'isDeleted'=>1));
        $emplois=array();
        if($pagination){
            foreach($jours as $jour){
                $sea=array();
                foreach($seances as $seance){
                    foreach($pagination as $ligne){
                        if(($ligne->getJour()->getId()==$jour->getId()) && ($ligne->getSeance()->getId()==$seance->getId())){
                            $sea[]=['matiere'=>$ligne->getMatiere(),'seance'=>$ligne->getSeance(),'emplois'=>$ligne,'user'=>$ligne->getUser(),'salle'=>$ligne->getSalle()];
                        }
                    }
                }
                $emplois[]=['jour'=>$jour,'seances'=>$sea];
            }
        }
        $template = 'base1.html.twig';
        $footer = 'footer.html.twig';
        $filename = sprintf('Facture-%s.pdf', date('d-m-Y'));
        $html = $this->renderView($template,array('classe'=>$classe,'dateD'=>$dateD,'dateF'=>$pagination[0]->getdateF(),'pagination'=>$emplois,'seances'=>$seances,'matieres'=>$matieres));
        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html, array(
                'orientation' => 'Landscape',
            ))
            ,
            200,
            [
                'Content-Type'        => 'application/pdf',
                'Content-Disposition' => sprintf('inline; filename="%s"', $filename),
            ]
        );
    }

    public function imprimerSemestreAction(Request $request,$classe,$semestre)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $classe1 = $em->getRepository('AppBundle:Classe')->find($classe);
        $jours = $em->getRepository('AppBundle:Jour')->findAll();
        $seances = $em->getRepository('AppBundle:Seance')->findAll();
        $matieres=$em->getRepository("AppBundle:Matiere")->findBy(array('classe'=>$classe));
        $emplois = $em->createQueryBuilder()
            ->select('c')
            ->from('AppBundle:Emplois', 'c')
            ->join('c.classe','a')
            ->join('c.semestre','s')
            ->where('a.id='.$classe)
            ->andwhere('s.id='.$semestre)
            ->andwhere('c.isDeleted=1')
            ->groupBy('c.dateD')
            ->getQuery()
            ->getResult();
        $res=array();
        foreach($emplois as $emp){
            $pagination = $em->getRepository('AppBundle:Emplois')->findBy(array('dateD'=>$emp->getDateD(),'isDeleted'=>1,'classe'=>$classe1));
            $page=array();
            if($pagination){
                foreach($jours as $jour){
                    $sea=array();
                    foreach($seances as $seance){
                        foreach($pagination as $ligne){
                            if(($ligne->getJour()->getId()==$jour->getId()) && ($ligne->getSeance()->getId()==$seance->getId())){
                                $sea[]=['matiere'=>$ligne->getMatiere(),'seance'=>$ligne->getSeance(),'emplois'=>$ligne,'user'=>$ligne->getUser(),'salle'=>$ligne->getSalle()];
                            }
                        }
                    }
                    $page[]=['jour'=>$jour,'seances'=>$sea];
                }
            }
            $res[]=['dateD'=>$emp->getDateD(),'dateF'=>$emp->getDateF(),'page'=>$page];
        }
        $template = 'base2.html.twig';
        $filename = sprintf('emplois-%s.pdf', date('d-m-Y'));
        $html = $this->renderView($template,array('classe'=>$classe1,'pagination'=>$res,'seances'=>$seances,'matieres'=>$matieres));
        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html, array(
                'orientation' => 'Landscape',
            )),
            200,
            [
                'Content-Type'        => 'application/pdf',
                'Content-Disposition' => sprintf('inline; filename="%s"', $filename),
            ]
        );
    }




}
