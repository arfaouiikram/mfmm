<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Annee;
use AppBundle\Entity\Classe;
use AppBundle\Entity\Devoir;
use AppBundle\Entity\Emplois;
use AppBundle\Entity\Filiere;
use AppBundle\Entity\Groupe;
use AppBundle\Entity\Matiere;
use AppBundle\Entity\Niveau;
use AppBundle\Entity\Semestre;
use AppBundle\Form\AnneeForm;
use AppBundle\Form\ClasseForm;
use AppBundle\Form\DevoirForm;
use AppBundle\Form\EmploisForm;
use AppBundle\Form\FiliereForm;
use AppBundle\Form\GroupeForm;
use AppBundle\Form\MatiereForm;
use AppBundle\Form\NiveauForm;
use AppBundle\Form\SemestreForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProfesseurController extends Controller
{

    public function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $users = $em->createQueryBuilder()
            ->select('u')
            ->from('AppBundle:User','u')
            ->where('u.isDeleted=1')
            ->getQuery()
            ->getResult();
        $role=$this->container->getParameter('security.role_hierarchy.roles');
            $pagination=array();
            foreach($users as $user){
                if($user->hasRole($role['ROLE_ENSEIGNANT'][0]) && !($user->isSuperAdmin())){
                    $pagination[]=$user;
                }
            }
        return $this->render("@AppBundle/Resources/views/professeur/list.html.twig",array('pagination' => $pagination));
    }


    public function ajoutAction(Request $request){
        $roles=$this->container->getParameter('security.role_hierarchy.roles');
        $role=$roles['ROLE_ENSEIGNANT'];
        $displayAgent='block';
        $displayEtudiant='block';
        $displayEnseignant='none';
        return $this->redirectToRoute("fos_user_registration_register",array('role'=>$role,'displayAgent'=>$displayAgent,'displayEnseignant'=>$displayEnseignant,'displayEtudiant'=>$displayEtudiant));
    }

    public function modifierAction(Request $request ,$id){
        $displayAgent='block';
        $displayEtudiant='block';
        $displayEnseignant='none';
        return $this->redirectToRoute("fos_user_profile_edit",array('user'=>$id,'displayAgent'=>$displayAgent,'displayEnseignant'=>$displayEnseignant,'displayEtudiant'=>$displayEtudiant));
    }
    public function supprimerAction(Request $request ,$id){
        $em = $this->get('doctrine.orm.entity_manager');
        $user = $em->getRepository('AppBundle:User')->find($id);
        $user->setIsDeleted(0);
        $em->persist($user);
        $em->flush();
        return $this->redirectToRoute("mfmm_crm_professeurs");

    }

    public function conventionAction(Request $request,$id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $user = $em->getRepository('AppBundle:User')->find($id);
        $template = 'convention.html.twig';
        $filename = sprintf('convention-%s.pdf', $user->getId());
        $html = $this->renderView($template,array('user'=>$user));
        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html)
            ,
            200,
            [
                'Content-Type'        => 'application/pdf',
                'Content-Disposition' => sprintf('inline; filename="%s"', $filename),
            ]
        );
    }




}
